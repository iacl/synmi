from __future__ import print_function, division

import os

import numpy as np

from .utils import get_channel_axis, reorient_3d_image


class InputGenerator(object):
    EXTENSIONS = None
    
    def __init__(self, pad_size=None, patch_size=(128, 128), flip=False, access='random', random_orient=False,
                 gen_final_activation=None, img_max=None, master=None, shift_range=(0, 0), channel_dropout=None,
                 channel_protect=(0,)):
        self.pad_size = tuple(pad_size) if pad_size is not None else None
        self.patch_size = tuple(patch_size)
        self.flip = flip
        self.access = access
        self.random_orient = random_orient
        self.gen_final_activation = gen_final_activation
        self.channel_dropout = channel_dropout
        self.channel_protect = channel_protect
        self.master = master
        self.shift_range = shift_range
        
        self.position = 0
        self.image_arrays = None
        self.img_max = img_max
        self.num_volumes = None
        self.img_shapes = None
        self.array_shapes = None
        self.indexes = None
        self.current_indexes = None

        self.patch2D = len(patch_size) == 2
        if self.patch2D:
            self.patch_size += (1,)
            if self.pad_size is not None:
                if self.random_orient:
                    self.pad_size = (max(self.pad_size),) * 3
                else:
                    self.pad_size = self.pad_size[:2] + (0,)

        self.crop_shifts = tuple([(int(np.floor(self.patch_size[i] / 2.0)), int(np.ceil(self.patch_size[i] / 2.0)))
                                  for i in range(len(self.patch_size))])
        
        self.orient_dict = {0: (1, 2, 0), 1: (0, 2, 1), 2: (0, 1, 2)}

    def is_matching_file(self, filename):
        return any(filename.lower().endswith(extension) for extension in self.EXTENSIONS)

    def crop(self, img, patch_center, orient):
        idx = self.orient_dict[orient]
        slices = [(int(patch_center[i] - self.crop_shifts[idx[i]][0]),
                   int(patch_center[i] + self.crop_shifts[idx[i]][1])) for i in range(3)]
        return img[slices[0][0]:slices[0][1], slices[1][0]:slices[1][1], slices[2][0]:slices[2][1], ...]

    def reset(self):
        self.position = 0
        
    def walk_dir(self, dirname):
        files = []
        for root, _, fnames in sorted(os.walk(dirname)):
            for fname in sorted(fnames):
                if self.is_matching_file(fname):
                    path = os.path.join(root, fname)
                    files.append(path)
        return files

    @staticmethod
    def read_image(imgs, idx):
        raise NotImplementedError

    def __call__(self, batch_size):
        if self.master is None or self.access == 'random':
            vol_indexes = np.random.choice(self.num_volumes, batch_size)
            orient_code = int(np.random.choice(range(3))) if self.random_orient else 2
            patch_centers = [tuple(self.indexes[vol_idx][np.random.randint(0, len(self.indexes[vol_idx]))])
                             for vol_idx in vol_indexes]
            self.current_indexes = [(vol_idx,) + centers + (orient_code,)
                                    for vol_idx, centers in zip(vol_indexes, patch_centers)]
        elif self.access == 'structured':
            self.current_indexes = []
            shifts = np.random.randint(-1*self.shift_range[0], self.shift_range[1]+1, batch_size)
            vol_indexes = np.random.choice(self.num_volumes, batch_size)
            for i, idx in enumerate(self.master.current_indexes):
                centers = [self.within_limits(idx[j + 1] + (shifts[i] if j == idx[4] else 0), vol_indexes[i], j, idx[4])
                           for j in range(3)]
                self.current_indexes.append((vol_indexes[i], centers[0], centers[1], centers[2], idx[4]))
        else:
            self.current_indexes = self.master.current_indexes
        batch = []
        for i, idx in enumerate(self.current_indexes):
            img = self.get_img(idx)
            img = self.crop(img, (idx[1], idx[2], idx[3]), idx[4])
            if self.patch2D:
                img = np.squeeze(img, axis=(2 if idx[4] == 2 else 1))
            if self.flip and idx[4] in [1, 2]:
                if i <= len(self.current_indexes)/2.0:
                    img = np.flip(img, 0)
            if self.gen_final_activation is not None:
                if self.gen_final_activation == 'tanh':
                    img = ((img / (self.img_max / 2.0)) - 1.0)
                elif self.gen_final_activation in ['normalized-relu', 'sigmoid']:
                    img = img / self.img_max
            if self.channel_dropout is not None and img.shape[2] > 1:
                if np.random.random() < self.channel_dropout:
                    drop_channel = np.random.choice([chan for chan in range(img.shape[2])
                                                    if chan not in self.channel_protect])
                    img[..., drop_channel] = np.zeros(img.shape[:-1])
            batch.append(img)
        batch = np.array(batch)
        if get_channel_axis() > 0:
            batch = np.transpose(batch, [0, 3, 1, 2])
        return batch
    
    def within_limits(self, idx, vol_index, axis, orient):
        axis = self.orient_dict[orient][axis]
        return int(max(self.crop_shifts[axis][0],
                       min(idx, self.array_shapes[vol_index][axis] - self.crop_shifts[axis][1] - 1)))
    
    def get_img(self, idx):
        if idx[4] == 2:
            return self.image_arrays[idx[0]]
        elif idx[4] in [0, 1]:
            return np.flip(self.image_arrays[idx[0]], 2)
        else:
            raise ValueError('Invalid Orientation Code')
    
    def get_nz_indexes(self, masks, target_orient):
        if masks is None:
            masks = [np.sum(img_arr, axis=-1) > 0 for img_arr in self.image_arrays]
        else:
            masks = [self.walk_dir(masks)]
            masks = self.get_image_arrays(masks, target_orient)
            masks = [mask > 0 for mask in masks]
        if self.pad_size is not None:
            patch_size = ((np.max(self.patch_size),) * 3) if self.random_orient else self.patch_size
            crop_shifts = [(int(np.floor(patch_size[i] / 2.0)),
                            int(np.ceil(patch_size[i] / 2.0))) for i in range(len(patch_size))]
            for i in range(len(self.image_arrays)):
                padded_size = [max(self.pad_size[j], self.img_shapes[i][j]) for j in range(3)]
                pad_dist = [padded_size[j] - self.img_shapes[i][j] for j in range(3)]
                crop_dist = [(int(np.floor(pad_dist[j]/2.0)), int(-1 * np.ceil(pad_dist[j]/2.0))) if pad_dist[j] > 0
                             else (0, self.img_shapes[i][j]) for j in range(3)]
                pad_mask = np.zeros(padded_size)
                pad_mask[crop_shifts[0][0]:-crop_shifts[0][1],
                         crop_shifts[1][0]:-crop_shifts[1][1],
                         crop_shifts[2][0]:-crop_shifts[2][1]] = 1.0
                masks[i] = masks[i] * pad_mask[crop_dist[0][0]:crop_dist[0][1],
                                               crop_dist[1][0]:crop_dist[1][1],
                                               crop_dist[2][0]:crop_dist[2][1]]
        self.indexes = [np.transpose(np.nonzero(mask)) for mask in masks]
        
    def pad_image_arrays(self):
        patch_size = ((np.max(self.patch_size),) * 3) if self.random_orient else self.patch_size
        max_shape = (np.max(self.img_shapes, axis=0) + np.array(patch_size)) if self.pad_size is None \
            else self.pad_size
        for i in range(len(self.image_arrays)):
            pad_size = [(max(0, max_shape[j] - self.image_arrays[i].shape[j])) for j in range(3)]
            pad_widths = tuple([(int(np.floor(pad/2.0)), int(np.ceil(pad/2.0))) for pad in pad_size]) + ((0, 0),)
            self.image_arrays[i] = np.pad(self.image_arrays[i], pad_width=pad_widths, mode='constant')
            if self.indexes is not None:
                self.indexes[i] = np.transpose(np.array([self.indexes[i][:, j] + pad_widths[j][0] for j in range(3)]))
            
    @staticmethod
    def get_image_arrays(filename_lists, target_orient):
        import nibabel as nib
    
        img_arrays = []
        for contrast_list in zip(*filename_lists):
            contrast_arrs = []
            # This loop restricts us to 3D image inputs and creates 4D input arrays [3D shape + (n_contrasts,)]
            for contrast in contrast_list:
                contrast_arrs.append(reorient_3d_image(nib.load(contrast), target_orient))
            img_arrays.append(np.stack(contrast_arrs, axis=get_channel_axis()))
        return img_arrays


class NiftiGenerator(InputGenerator):
    EXTENSIONS = ['.nii.gz', '.nii']
    
    def __init__(self, root_dirs, training_orient='RAI', pad_size=None, patch_size=(128, 128), flip=False,
                 access='random', random_orient=False, gen_final_activation=None, img_max=None, masks=None, master=None,
                 shift_range=(0, 0), channel_dropout=None, channel_protect=(0,)):
        super(NiftiGenerator, self).__init__(pad_size, patch_size, flip, access, random_orient, gen_final_activation,
                                             img_max, master, shift_range, channel_dropout, channel_protect)
        
        nifti_lists = []
        for root_dir in root_dirs:
            nifti_lists.append(self.walk_dir(root_dir))
        if not all([len(nifti_lists[i]) == len(nifti_lists[0]) for i in range(len(nifti_lists))]):
            raise ValueError('All image lists must be the same length.')
        self.image_arrays = self.get_image_arrays(nifti_lists, training_orient)
        if self.img_max is None:
            self.img_max = np.max([np.max(arr) for arr in self.image_arrays])
            if self.master is not None:
                self.img_max = np.max([self.img_max, self.master.img_max])
                self.master.img_max = self.img_max
        self.num_volumes = len(self.image_arrays)
        self.img_shapes = [arr.shape[:3] for arr in self.image_arrays]
        
        if self.master is None or self.access == 'random':
            self.get_nz_indexes(masks, training_orient)
        
        self.pad_image_arrays()
        self.array_shapes = [arr.shape[:3] for arr in self.image_arrays]
    
    @staticmethod
    def read_image(imgs, idx):
        return imgs[idx]


class PredictionGenerator(object):
    def __init__(self, patch_size=(128, 128)):
        self.patch_size = tuple(patch_size)
        self.patch2D = len(self.patch_size) == 2
        if self.patch2D:
            self.patch_size += (1, )
        self.position = 0


class NiftiPredictionGenerator(PredictionGenerator):
    def __init__(self, nifti_files, patch_size=(128, 128), gen_final_activation=None, img_max=None,
                 target_orient='RAI'):
        import nibabel as nib
        super(NiftiPredictionGenerator, self).__init__(patch_size)
        
        images = []
        for nifti_file in nifti_files:
            self.img_obj = nib.load(nifti_file)
            self.orig_orient = nib.aff2axcodes(self.img_obj.affine)
            images.append(reorient_3d_image(self.img_obj, target_orient))
        self.images = np.stack(images, get_channel_axis())
        del images
        
        pad_size = tuple([max(0, self.patch_size[i] - self.images.shape[i]) for i in range(3)])
        self.pad_widths = tuple([(int(np.floor(pad/2.0)), int(np.ceil(pad/2.0))) for pad in pad_size]) + ((0, 0),)
        self.images = np.pad(self.images, pad_width=self.pad_widths, mode='constant')
        
        self.img_max = np.max(self.images) if img_max is None else img_max
        self.img_size = self.images.shape[:3]
        num_patches = (np.ceil(np.array(self.img_size) / np.array(self.patch_size)) * 2) + 1
        num_patches = [1 if self.patch_size[i] == self.img_size[i] else num_patches[i] for i in range(3)]
        num_patches = [self.img_size[i] if self.patch_size[i] == 1 else num_patches[i] for i in range(3)]
        
        self.indexes = []
        self.patches = []
        for x_start in np.around(np.linspace(0, self.img_size[0] - self.patch_size[0], num_patches[0])).astype(np.int):
            x_end = x_start + self.patch_size[0]
            for y_start in np.around(np.linspace(0, self.img_size[1] - self.patch_size[1],
                                                 num_patches[1])).astype(np.int):
                y_end = y_start + self.patch_size[1]
                for z_start in np.around(np.linspace(0, self.img_size[2] - self.patch_size[2],
                                                     num_patches[2])).astype(np.int):
                    z_end = z_start + self.patch_size[2]
                    self.indexes.append((x_start, y_start, z_start))
                    patch = self.images[x_start:x_end, y_start:y_end, z_start:z_end, ...]
                    if self.patch2D:
                        patch = np.squeeze(patch, axis=2)
                    if len(patch.shape) < 3:
                        patch = np.reshape(patch, patch.shape + (1,))
                    if gen_final_activation is not None:
                        if gen_final_activation == 'tanh':
                            patch = (patch / (self.img_max / 2.0)) - 1.0
                        elif gen_final_activation in ['normalized-relu', 'sigmoid']:
                            patch = patch / self.img_max
                    self.patches.append(patch)
        self.patches = np.array(self.patches)
    
    def __call__(self, batch_size):
        batch = self.patches[self.position:min([self.position + batch_size, len(self.patches)])]
        self.position += batch_size
        return batch, self.position <= len(self.patches)
