import os

import numpy as np

from .utils import split_filename, get_image_data_format


def undo_reorient(data, orig_orient, target_orient):
    import nibabel as nib
    orient_dict = {'R': 'L', 'A': 'P', 'I': 'S', 'L': 'R', 'P': 'A', 'S': 'I'}
    
    target_orient = [orient_dict[char] for char in target_orient]
    targ_ornt = nib.orientations.axcodes2ornt(target_orient)
    orig_ornt = nib.orientations.axcodes2ornt(orig_orient)
    ornt_xfm = nib.orientations.ornt_transform(targ_ornt, orig_ornt)
    return nib.orientations.apply_orientation(data, ornt_xfm).astype(np.float32)


def unpad(img, padding):
    indexes = [(padding[i][0], (img.shape[i] if padding[i][1] == 0 else (-1 * padding[i][1]))) for i in range(3)]
    return img[indexes[0][0]:indexes[0][1], indexes[1][0]:indexes[1][1], indexes[2][0]:indexes[2][1], ...]
    

class OutputSink(object):
    def __init__(self):
        pass
    
    def __call__(self, result_images):
        raise NotImplementedError


class NiftiOutputSink(OutputSink):
    def __init__(self, output_filename, patch2d, crop_size, img_obj, img_size, img_max, indexes, pad_widths,
                 gen_final_activation, target_orient):
        super(NiftiOutputSink, self).__init__()
        self.output_dir, self.output_base, _ = split_filename(output_filename)
        self.patch2D = patch2d
        self.crop_size = crop_size
        self.img_obj = img_obj
        self.img_size = img_size
        self.img_max = img_max
        self.indexes = indexes
        self.pad_widths = pad_widths
        self.gen_final_activation = gen_final_activation
        self.target_orient = target_orient
    
    def __call__(self, result_images):
        import nibabel as nib
        
        if get_image_data_format() == 'channels_first':
            result_images = np.transpose(result_images, [0, 2, 3, 1])
        patch_size = (result_images.shape[1:3] + (1,)) if self.patch2D else result_images.shape[1:4]
        n_channels = result_images.shape[-1]
        
        img_size = self.img_size[:3]
        limit = np.array(img_size) - np.array(patch_size)
        image = np.zeros(img_size + (n_channels,))
        count = np.zeros(img_size + (n_channels,))
        for patch_num in range(len(self.indexes)):
            pos = self.indexes[patch_num]
            x_min = (pos[0] + self.crop_size[0]) if pos[0] != 0 else 0
            y_min = (pos[1] + self.crop_size[1]) if pos[1] != 0 else 0
            z_min = (pos[2] + self.crop_size[2]) if pos[2] != 0 else 0
            x_max = (pos[0] + patch_size[0] - self.crop_size[0]) if pos[0] != limit[0] else img_size[0]
            y_max = (pos[1] + patch_size[1] - self.crop_size[1]) if pos[1] != limit[1] else img_size[1]
            z_max = (pos[2] + patch_size[2] - self.crop_size[2]) if pos[2] != limit[2] else img_size[2]
            
            result = result_images[patch_num, ...]
            if self.patch2D:
                result = np.expand_dims(result, axis=2)
            patch = result[x_min - pos[0]:x_max - pos[0], y_min - pos[1]:y_max - pos[1],
                           z_min - pos[2]:z_max - pos[2], ...]
            image[x_min:x_max, y_min:y_max, z_min:z_max, :] = image[x_min:x_max, y_min:y_max, z_min:z_max, :] + patch
            count[x_min:x_max, y_min:y_max, z_min:z_max, :] = (count[x_min:x_max, y_min:y_max, z_min:z_max, :] +
                                                               np.ones((x_max - x_min, y_max - y_min, z_max-z_min,
                                                                        n_channels)))
        image /= count
        if self.gen_final_activation is not None:
            if self.gen_final_activation == 'tanh':
                image = (image + 1.0) * (self.img_max / 2.0)
            elif self.gen_final_activation in ['normalized-relu', 'sigmoid']:
                image *= self.img_max
        image = unpad(image, self.pad_widths)
        image = np.squeeze(image)

        image = undo_reorient(image, nib.aff2axcodes(self.img_obj.affine), self.target_orient)
        
        nib.Nifti1Image(image, self.img_obj.affine, self.img_obj.header).to_filename(
            os.path.join(self.output_dir, self.output_base + '.nii.gz'))
