import argparse

from synmi.models import PredictionModel
from synmi.generators import NiftiPredictionGenerator
from synmi.sinks import NiftiOutputSink

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    main = parser.add_argument_group('main')
    main.add_argument('--input-file', required=True, nargs='+', help='path to image file')
    main.add_argument('--output-file', required=True, help='path to save result image to')
    main.add_argument('--model-file', type=str, required=True, help='path to model definition file')
    main.add_argument('--batch-size', type=int, default=32, help='number of images to predict at once')
    
    images = parser.add_argument_group('images')
    images.add_argument('--orientation', type=str, choices=['axial', 'coronal', 'sagittal'], default='axial',
                        help='For Nifti images, orient to this orientation before slicing')
    images.add_argument('--patch-size', type=int, nargs='+', default=(128, 128), help='then crop to this size')
    images.add_argument('--gen-final-activation', type=str,
                        choices=[None, 'relu', 'normalized-relu', 'sigmoid', 'tanh'],
                        default='relu',
                        help='final activation function for generator (determines image normalization)')
    images.add_argument('--input-img-max', type=float, help='input image max to rescale inputs', default=1.0)
    images.add_argument('--output-img-max', type=float, help='output image max to rescale outputs', default=1.0)
    images.add_argument('--crop-size', type=int, nargs='+', default=(0, 0, 0), help='crop from patches when combining')
    
    args = parser.parse_args()

    orientations = {'axial': 'RAI', 'coronal': 'RSA', 'sagittal': 'ASR'}
    args.orientation = orientations[args.orientation]
    
    input_generator = NiftiPredictionGenerator(args.input_file, args.patch_size,
                                               gen_final_activation=args.gen_final_activation,
                                               img_max=args.input_img_max,
                                               target_orient=args.orientation)
    output_sink = NiftiOutputSink(args.output_file, input_generator.patch2D, args.crop_size, input_generator.img_obj,
                                  input_generator.img_size, args.output_img_max, input_generator.indexes,
                                  input_generator.pad_widths, args.gen_final_activation, args.orientation)
    
    predict_model = PredictionModel(args.model_file)
    predict_model.predict(input_generator, output_sink, args.batch_size)
