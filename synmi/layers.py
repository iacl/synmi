from keras import backend
import tensorflow as tf
from keras.layers import Conv2D, BatchNormalization, ZeroPadding2D, ZeroPadding3D, Layer

from .utils import get_image_data_format


class Subpixel(Conv2D):
    def __init__(self,
                 filters,
                 kernel_size,
                 r,
                 padding='valid',
                 data_format=None,
                 strides=(1, 1),
                 activation=None,
                 use_bias=True,
                 kernel_initializer='glorot_uniform',
                 bias_initializer='zeros',
                 kernel_regularizer=None,
                 bias_regularizer=None,
                 activity_regularizer=None,
                 kernel_constraint=None,
                 bias_constraint=None,
                 **kwargs):
        super(Subpixel, self).__init__(
            filters=r*r*filters,
            kernel_size=kernel_size,
            strides=strides,
            padding=padding,
            data_format=data_format,
            activation=activation,
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            bias_initializer=bias_initializer,
            kernel_regularizer=kernel_regularizer,
            bias_regularizer=bias_regularizer,
            activity_regularizer=activity_regularizer,
            kernel_constraint=kernel_constraint,
            bias_constraint=bias_constraint,
            **kwargs)
        self.r = r

    def _phase_shift(self, arr):
        r = self.r
        bsize, a, b, c = arr.get_shape().as_list()
        bsize = backend.shape(arr)[0]  # Handling Dimension(None) type for undefined batch dim
        x = backend.reshape(arr, [bsize, a, b, int(c/(r*r)), r, r])  # bsize, a, b, c/(r*r), r, r
        x = backend.permute_dimensions(x, (0, 1, 2, 5, 4, 3))  # bsize, a, b, r, r, c/(r*r)
        # backend does not support tf.split, so in future versions this could be nicer
        x = [x[:, i, :, :, :, :] for i in range(a)]  # a, [bsize, b, r, r, c/(r*r)
        x = backend.concatenate(x, 2)  # bsize, b, a*r, r, c/(r*r)
        x = [x[:, i, :, :, :] for i in range(b)]  # b, [bsize, r, r, c/(r*r)
        x = backend.concatenate(x, 2)  # bsize, a*r, b*r, c/(r*r)
        return x

    def call(self, inputs, **kwargs):
        return self._phase_shift(super(Subpixel, self).call(inputs))

    def compute_output_shape(self, input_shape):
        unshifted = super(Subpixel, self).compute_output_shape(input_shape)
        return unshifted[0], self.r*unshifted[1], self.r*unshifted[2], int(unshifted[3]/(self.r*self.r))

    def get_config(self):
        config = super(Conv2D, self).get_config()
        config.pop('rank')
        config.pop('dilation_rate')
        config['filters'] /= self.r*self.r
        config['r'] = self.r
        return config


class BatchNormalizationGAN(BatchNormalization):
    
    def __call__(self, inputs, training=None):
        return super(BatchNormalizationGAN, self).__call__(inputs, training=True)


class ReflectionPadding2D(ZeroPadding2D):
    def call(self, inputs):
        if get_image_data_format() == 'channels_first':
            pattern = [[0, 0],
                       [0, 0],
                       [self.padding[0][0], self.padding[0][1]],
                       [self.padding[1][0], self.padding[1][1]]]
        else:
            pattern = [[0, 0],
                       [self.padding[0][0], self.padding[0][1]],
                       [self.padding[1][0], self.padding[1][1]],
                       [0, 0]]
        return tf.pad(inputs, pattern, mode='REFLECT')


class ReflectionPadding3D(ZeroPadding3D):
    def call(self, inputs):
        if get_image_data_format() == 'channels_first':
            pattern = [[0, 0],
                       [0, 0],
                       [self.padding[0][0], self.padding[0][1]],
                       [self.padding[1][0], self.padding[1][1]],
                       [self.padding[2][0], self.padding[2][1]]]
        else:
            pattern = [[0, 0],
                       [self.padding[0][0], self.padding[0][1]],
                       [self.padding[1][0], self.padding[1][1]],
                       [self.padding[2][0], self.padding[2][1]],
                       [0, 0]]
        return tf.pad(inputs, pattern, mode='REFLECT')


# Borrowed from https://github.com/Shaofanl/Keras-GAN/ (modified by Blake Dewey)
class GradNorm(Layer):
    def __init__(self, **kwargs):
        super(GradNorm, self).__init__(**kwargs)
    
    def build(self, input_shapes):
        super(GradNorm, self).build(input_shapes)
    
    def call(self, inputs, **kwargs):
        target, wrt = inputs
        grads = backend.gradients(target, wrt)
        assert len(grads) == 1
        grad = grads[0]
        grad_norm = backend.sqrt(backend.sum(backend.square(grad), axis=backend.arange(1, len(grad.shape))))
        return grad_norm - 1.0
    
    def compute_output_shape(self, input_shapes):
        return input_shapes[1][0], 1


class GradNormLP(GradNorm):
    def __init__(self, **kwargs):
        super(GradNormLP, self).__init__(**kwargs)
    
    def build(self, input_shapes):
        super(GradNormLP, self).build(input_shapes)
    
    def call(self, inputs, **kwargs):
        grad_norm = super(GradNormLP, self).call(inputs, **kwargs)
        return backend.maximum(grad_norm, 0.0)
    
    def compute_output_shape(self, input_shapes):
        return input_shapes[1][0], 1
