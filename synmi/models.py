from __future__ import print_function, division

import os
from functools import partial
import time
from glob import glob
from collections import OrderedDict
import json
import math

import numpy as np
import tensorflow as tf

from keras import backend
from keras.models import Input, Model, load_model
from keras.optimizers import Adam, RMSprop
from keras.layers import Concatenate
from keras.utils.multi_gpu_utils import multi_gpu_model
from keras_contrib.layers.normalization import InstanceNormalization

from .networks import build_generator_model, build_discriminator_model
from .visualization import save_training_page
from .utils import get_input_shape, get_channel_axis, mkdir_p, writeline
from .layers import ReflectionPadding2D, ReflectionPadding3D, GradNorm, GradNormLP


class BaseModelSpec(object):
    def __init__(self, name, model_type, inout):
        self.name = name
        self.model_type = model_type
        self.inout = inout
        
        
class OuterModelSpec(object):
    def __init__(self, loss_functions, loss_weights, loss_names):
        self.loss_functions = loss_functions
        self.loss_weights = loss_weights
        self.loss_names = loss_names
        

class CallableEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, OuterModelSpec) or isinstance(obj, BaseModelSpec):
            return obj.__dict__
        elif callable(obj) or hasattr(obj, '__name__'):
            return obj.__name__
        elif hasattr(obj, '__class__'):
            return obj.__class__.__name__
        return super(CallableEncoder, self).default(obj)


SYNMI_CUSTOM_OBJECTS = {'InstanceNormalization': InstanceNormalization, 'ReflectionPadding2D': ReflectionPadding2D,
                        'ReflectionPadding3D': ReflectionPadding3D}


class BaseModel(object):
    inputs = None
    base_models = None
    outer_models = None
    
    def __init__(self, **kwargs):
        self.patch_size = tuple(kwargs.get('patch_size', (128, 128)))
        self.patch2D = len(self.patch_size) == 2
        self.input_nc = kwargs.get('input_nc', 3)
        self.input_patch_size = get_input_shape(self.patch2D, self.input_nc)
        self.output_nc = kwargs.get('output_nc', 3)
        self.output_patch_size = get_input_shape(self.patch2D, self.output_nc)
        self.z_res_change = kwargs.get('z_res_change', 'all')
        
        self.generator_name = kwargs.get('generator_name', 'unet')
        self.generator_levels = kwargs.get('generator_levels', 4)
        self.generator_blocks = kwargs.get('generator_blocks', 2)
        self.generator_loss = kwargs.get('generator_loss', 'MAE')
        self.init_num_filters_gen = kwargs.get('init_filters_gen', 64)
        self.downsample_method_gen = kwargs.get('downsample_method_gen', 'max_pool')
        self.upsample_method_gen = kwargs.get('upsample_method_gen', 'upsample')
        self.conv_activation_gen = kwargs.get('conv_activation_gen', 'relu')
        self.kernel_init_gen = kwargs.get('kernel_initializer_gen', 'he_normal')
        self.padding_gen = kwargs.get('padding_gen', 'zero')
        self.norm_layer_gen = kwargs.get('norm_method_gen', 'instance')
        self.skip_connections_gen = kwargs.get('skip_connections_gen', 'all')
        self.resblocks = kwargs.get('resblocks', False)
        self.gen_final_activation = kwargs.get('gen_final_activation', 'relu')
        
        optimizer_str = kwargs.get('optimizer', 'Adam')
        self.lr = kwargs.get('learning_rate', 0.001)
        self.beta1 = kwargs.get('beta1', 0.9)
        self.beta2 = kwargs.get('beta2', 0.999)
        if optimizer_str == 'Adam':
            self.optimizer = Adam(lr=self.lr, beta_1=self.beta1, beta_2=self.beta2)
        elif optimizer_str == 'RMSprop':
            self.optimizer = RMSprop(lr=self.lr, rho=self.beta1)
        
        self.model_dir = kwargs.get('model_dir')
        self.experiment_name = kwargs.get('experiment_name')
        
        self.num_gpus = kwargs.get('num_gpus', 1)
        self.gpu_ids = kwargs.get('gpu_ids', None)
        self.cpu_merge = kwargs.get('cpu_merge', False)

        self.inputs_summary(kwargs, vars(self))
        
        if self.num_gpus > 1:
            self.merge_device = '/cpu:0' if self.cpu_merge else ('/gpu:%d' % self.gpu_ids[0])
        else:
            self.merge_device = '/gpu:0'
        
        if self.model_dir is None or self.experiment_name is None:
            raise RuntimeError('Model Directory and Experiment Name are both required.')
        
        self.log_file = os.path.join(self.model_dir, self.experiment_name + '.log')

        with tf.device(self.merge_device):
            if kwargs.get('continue_training', False):
                self.exp_to_load = kwargs.get('experiment_to_load', self.experiment_name)
                self.which_epoch = kwargs.get('which_epoch', 'latest')
                self.load_models(self.model_dir, self.exp_to_load, self.which_epoch)
            else:
                self.build_base_models()

        self.build_outer_models()
        
        self.base_model_summaries()
        self.outer_model_summaries()
    
    @staticmethod
    def make_trainable(net, val):
        net.trainable = val
        for l in net.layers:
            l.trainable = val
            
    def print_losses(self, message, names, losses):
        for name, loss in zip(names, losses):
            message += '%s: %s ' % (name, ('%.7g' % loss).ljust(12))
        self.print_message(message)
    
    def print_message(self, message):
        with open(self.log_file, 'a') as log_file:
            log_file.write(message + '\n')
            
    def inputs_summary(self, inputs, modified_inputs):
        inputs = {key: inputs[key] for key in sorted(inputs.keys()) if key != 'message_type'}
        modified_inputs = {key: modified_inputs[key] for key in sorted(modified_inputs.keys())}
        with open(os.path.join(self.model_dir, self.experiment_name + '_inputs.json'), 'w') as out_file:
            out_file.write('================ Raw Inputs ================\n')
            out_file.write(json.dumps(inputs, indent=4, sort_keys=True, cls=CallableEncoder) + '\n')
            out_file.write('================ Modified Inputs ================\n')
            out_file.write(json.dumps(modified_inputs, indent=4, sort_keys=True, cls=CallableEncoder) + '\n')
            
    def base_model_summaries(self):
        with open(os.path.join(self.model_dir, self.experiment_name + '_base_models.txt'), 'w') as out_file:
            write_func = partial(writeline, out_file)
            for model_arg, model_spec in self.base_models.items():
                out_file.write('================ %s (%s) ================\n' % (model_spec.name, model_arg))
                write_model = self.build_base_model(model_arg, model_spec, True)
                write_model.summary(print_fn=write_func, line_length=120, positions=[48, 80, 88, 120])
                
    def outer_model_summaries(self):
        with open(os.path.join(self.model_dir, self.experiment_name + '_outer_models.txt'), 'w') as out_file:
            write_func = partial(writeline, out_file)
            for model_arg in self.outer_models.keys():
                out_file.write('================ %s ================\n' % model_arg)
                getattr(self, model_arg).summary(print_fn=write_func, line_length=120, positions=[48, 80, 88, 120])
            
    def load_models(self, model_dir, exp_to_load, which_epoch):
        if which_epoch == 'latest':
            g_a_models = sorted(glob(os.path.join(model_dir, exp_to_load + '_checkpoints',
                                                  exp_to_load + '_' + self.base_models[0][1] + '_epoch*.h5')))
            epoch_number = int(str(os.path.basename(g_a_models[-1]).split('_')[-1]).split('.')[0].replace('epoch', ''))
        else:
            epoch_number = int(which_epoch)
        for model_arg, model_spec in self.base_models.items():
            model_file = os.path.join(model_dir, exp_to_load + '_checkpoints',
                                      '%s_%s_epoch%03d.h5' % (exp_to_load, model_spec.name, epoch_number))
            self.print_message('Loading ' + model_spec.name + ' from file: ' + model_file)
            setattr(self, model_arg, load_model(model_file, custom_objects=SYNMI_CUSTOM_OBJECTS))
            getattr(self, model_arg).name = model_arg
    
    def build_base_model(self, model_arg, model_spec, for_summary=False):
        if model_spec.model_type == 'gen':
            input_nc, output_nc = tuple([getattr(self, selection) for selection in model_spec.inout])
            input_patch_size = (self.patch_size if for_summary else ((None, ) * (2 if self.patch2D else 3))) \
                + (input_nc, )
            return build_generator_model(self.patch2D, input_patch_size, output_nc, self.init_num_filters_gen,
                                         self.generator_name, self.generator_levels, self.generator_blocks,
                                         self.downsample_method_gen, self.upsample_method_gen, self.z_res_change,
                                         self.conv_activation_gen, self.kernel_init_gen, self.padding_gen,
                                         self.norm_layer_gen, self.skip_connections_gen, self.resblocks,
                                         self.gen_final_activation)
        else:
            raise ValueError('Model type %s is not valid.' % model_spec.model_type)
    
    def build_base_models(self):
        for model_arg, model_spec in self.base_models.items():
            self.print_message('Building ' + model_spec.name + ' from input specifications.')
            setattr(self, model_arg, self.build_base_model(model_arg, model_spec))
            getattr(self, model_arg).name = model_spec.name
            
    def decay_learning_rate(self, epoch, total_epochs):
        current_lr = self.lr - (epoch * self.lr / total_epochs)
        for model_arg in self.outer_models.keys():
            backend.set_value(getattr(self, model_arg).optimizer.lr, current_lr)

    def connect_inputs(self, **kwargs):
        for input_name, input_generator in kwargs.items():
            if input_name not in self.inputs:
                raise ValueError('Input %s not valid for this class. [%s]' % (input_name, str(self.inputs)))
            setattr(self, input_name, input_generator)
            
    def loss_names(self):
        return [item for model_spec in self.outer_models.values() for item in model_spec.loss_names]
            
    def compile(self, model_arg):
        output_names = self.outer_models[model_arg].loss_names[1:] if len(self.outer_models[model_arg].loss_names) > 1 \
            else self.outer_models[model_arg].loss_names
        getattr(self, model_arg).output_names = output_names
        if self.num_gpus > 1:
            setattr(self, model_arg, multi_gpu_model(getattr(self, model_arg), self.gpu_ids,
                                                     cpu_merge=self.cpu_merge))
        getattr(self, model_arg).compile(optimizer=self.optimizer, loss=self.outer_models[model_arg].loss_functions,
                                         loss_weights=self.outer_models[model_arg].loss_weights)
    
    def save_models(self, epoch):
        mkdir_p(os.path.join(self.model_dir, self.experiment_name + '_checkpoints'))
        for model_arg, model_spec in self.base_models.items():
            model_file = os.path.join(self.model_dir, self.experiment_name + '_checkpoints',
                                      '%s_%s_epoch%03d.h5' % (self.experiment_name, model_spec.name, epoch))
            self.print_message('Saving ' + model_spec.name + (' to file for epoch %03d : ' % epoch) + model_file)
            getattr(self, model_arg).save(model_file)
            
    def build_outer_models(self):
        raise NotImplementedError
    
    def fit(self, batch_size, n_epochs, n_epochs_decay, steps_per_epoch, save_freq, print_freq, starting_epochs):
        raise NotImplementedError
        

class SingleCovNet(BaseModel):
    
    def __init__(self, **kwargs):
        self.input_a = None
        self.input_b = None
        self.generator = None
        self.model = None

        generator_loss = kwargs.get('generator_loss', 'MAE')
        
        self.inputs = ['input_a', 'input_b']
        self.base_models = OrderedDict([('generator', BaseModelSpec('generator', 'gen', ('input_nc', 'output_nc')))])
        self.outer_models = OrderedDict([('model', OuterModelSpec([generator_loss], [1.0], ['Loss']))])

        super(SingleCovNet, self).__init__(**kwargs)
        
    def build_outer_models(self):
        self.model = self.generator
        self.compile('model')

    def fit(self, batch_size, n_epochs, n_epochs_decay, steps_per_epoch,
            save_freq, print_freq, starting_epoch):
    
        self.print_message('================ Training Loss (%s) ================' % time.strftime('%c'))
        
        batch_size = batch_size * self.num_gpus
        steps_per_epoch = steps_per_epoch // self.num_gpus
    
        total_steps = 0
        for epoch in range(starting_epoch, n_epochs + n_epochs_decay + 1):
            epoch_start_time = time.time()
            if epoch > n_epochs:
                self.decay_learning_rate(epoch - n_epochs, n_epochs_decay)
        
            iter_losses = []
            losses = []
            for i in range(steps_per_epoch):
                iter_start_time = time.time()
                total_steps += 1
            
                real_a = self.input_a(batch_size)
                real_b = self.input_b(batch_size)
                model_loss = self.model.train_on_batch([real_a], [real_b])
            
                iter_losses.append([model_loss])
            
                if total_steps % print_freq == 0:
                    mean_iter_loss = []
                    for loss in zip(*iter_losses):
                        mean_iter_loss.append(np.mean(loss))
                    losses.extend(iter_losses)
                    iter_losses = []
                    time_per_img = (time.time() - iter_start_time) / batch_size
                    message = '(epoch: %d, iters: %d, time: %.3f) ' % (epoch, i + 1, time_per_img)
                    self.print_losses(message, self.loss_names(), mean_iter_loss)
                
                    fake_b = self.model.predict(real_a)
                    visuals = OrderedDict([('real_A', real_a[0, ...]), ('fake_B', fake_b[0, ...]),
                                           ('real_B', real_b[0, ...])])
                    save_training_page(self.model_dir, self.experiment_name, visuals, epoch)
        
            losses.extend(iter_losses)
            mean_loss = []
            for loss in zip(*losses):
                mean_loss.append(np.mean(loss))
        
            self.print_message('End of epoch %d / %d \t Time Elapsed: %d sec' % (epoch, n_epochs + n_epochs_decay,
                                                                                 time.time() - epoch_start_time))
            message = 'Epoch %d Losses: ' % epoch
            self.print_losses(message, self.loss_names(), mean_loss)
        
            if epoch % save_freq == 0:
                self.save_models(epoch)


class CycleNoGAN(BaseModel):
    
    def __init__(self, **kwargs):
        self.use_identity_loss = kwargs.get('use_identity_loss', False)
        lambda_a = kwargs.get('lambda_a', 10.0)
        lambda_b = kwargs.get('lambda_b', 10.0)
        id_lambda = kwargs.get('id_lambda', 0.0)
        generator_loss = kwargs.get('generator_loss', 'MAE')

        self.input_a = None
        self.input_b = None
        self.gen_a = None
        self.gen_b = None
        self.generative_model = None
        
        if self.use_identity_loss and kwargs.get('input_nc') != kwargs.get('output_nc'):
            raise ValueError('Identity mapping is not supported with unequal channels between inputs and outputs.')

        if self.use_identity_loss:
            loss_weights = [1.0, 1.0, lambda_a, lambda_b, id_lambda * lambda_a, id_lambda * lambda_b]
            loss_functions = [generator_loss, generator_loss, generator_loss, generator_loss, generator_loss,
                              generator_loss]
            loss_names = ['Total_Loss', 'G_A', 'G_B', 'Cyc_A', 'Cyc_B', 'Id_A', 'Id_B']
        else:
            loss_weights = [1.0, 1.0, lambda_a, lambda_b]
            loss_functions = [generator_loss, generator_loss, generator_loss, generator_loss]
            loss_names = ['Total_Loss', 'G_A', 'G_B', 'Cyc_A', 'Cyc_B']
            
        self.inputs = ['input_a', 'input_b']
        self.base_models = OrderedDict([('gen_a', BaseModelSpec('G_A', 'gen', ('input_nc', 'output_nc'))),
                                        ('gen_b', BaseModelSpec('G_B', 'gen', ('output_nc', 'input_nc')))])
        self.outer_models = OrderedDict([('generative_model', OuterModelSpec(loss_functions, loss_weights,
                                                                             loss_names))])
        
        super(CycleNoGAN, self).__init__(**kwargs)
    
    def build_outer_models(self):
        # Build generative model
        with tf.device(self.merge_device):
            real_a = Input(shape=self.input_patch_size)  # A
            fake_b = self.gen_a(real_a)  # B' = G_A(A)
            recon_a = self.gen_b(fake_b)  # A'' = G_B(G_A(A))
        
            real_b = Input(shape=self.output_patch_size)  # B
            fake_a = self.gen_b(real_b)  # A' = G_B(B)
            recon_b = self.gen_a(fake_a)  # B'' = G_A(G_B(B))
        
            if self.use_identity_loss:
                id_a = self.gen_b(real_a)  # I' = G_B(A)
                id_b = self.gen_a(real_b)  # I' = G_A(B)
                self.generative_model = Model([real_a, real_b], [fake_a, fake_b, recon_a, recon_b, id_a, id_b])
            else:
                self.generative_model = Model([real_a, real_b], [fake_a, fake_b, recon_a, recon_b])

        self.compile('generative_model')
    
    def fit(self, batch_size, n_epochs, n_epochs_decay, steps_per_epoch,
            save_freq, print_freq, starting_epoch):
    
        self.print_message('================ Training Loss (%s) ================' % time.strftime('%c'))

        batch_size = batch_size * self.num_gpus
        steps_per_epoch = steps_per_epoch // self.num_gpus
        
        total_steps = 0
        for epoch in range(starting_epoch, n_epochs + n_epochs_decay + 1):
            epoch_start_time = time.time()
            if epoch > n_epochs:
                self.decay_learning_rate(epoch - n_epochs, n_epochs_decay)
            
            iter_losses = []
            losses = []
            for i in range(steps_per_epoch):
                iter_start_time = time.time()
                total_steps += 1

                real_a = self.input_a(batch_size)
                real_b = self.input_b(batch_size)
                if self.use_identity_loss:
                    model_loss = self.generative_model.train_on_batch([real_a, real_b], [real_a, real_b, real_a,
                                                                                         real_b, real_a, real_b])
                else:
                    model_loss = self.generative_model.train_on_batch([real_a, real_b], [real_a, real_b,
                                                                                         real_a, real_b])
                iter_losses.append(model_loss)
                
                if total_steps % print_freq == 0:
                    mean_iter_loss = []
                    for loss in zip(*iter_losses):
                        mean_iter_loss.append(np.mean(loss))
                    losses.extend(iter_losses)
                    iter_losses = []
                    time_per_img = (time.time() - iter_start_time) / batch_size
                    message = '(epoch: %d, iters: %d, time: %.3f) ' % (epoch, i + 1, time_per_img)
                    self.print_losses(message, self.loss_names(), mean_iter_loss)

                    fake_a = self.gen_b.predict(real_b)
                    fake_b = self.gen_a.predict(real_a)
                    rec_a = self.gen_b.predict(fake_b)
                    rec_b = self.gen_a.predict(fake_a)
                    if self.use_identity_loss:
                        id_a = self.gen_b.predict(real_a)
                        id_b = self.gen_a.predict(real_b)
                        visuals = OrderedDict([('real_A', real_a[0, ...]), ('fake_A', fake_a[0, ...]),
                                               ('rec_A', rec_a[0, ...]), ('idt_A', id_a[0, ...]),
                                               ('real_B', real_b[0, ...]), ('fake_B', fake_b[0, ...]),
                                               ('rec_B', rec_b[0, ...]), ('idt_b', id_b[0, ...])])
                    else:
                        visuals = OrderedDict([('real_A', real_a[0, ...]), ('fake_A', fake_a[0, ...]),
                                               ('rec_A', rec_a[0, ...]), ('real_B', real_b[0, ...]),
                                               ('fake_B', fake_b[0, ...]), ('rec_B', rec_b[0, ...])])
                    save_training_page(self.model_dir, self.experiment_name, visuals, epoch)
            
            losses.extend(iter_losses)
            mean_loss = []
            for loss in zip(*losses):
                mean_loss.append(np.mean(loss))
            
            self.print_message('End of epoch %d / %d \t Time Elapsed: %d sec' % (epoch, n_epochs + n_epochs_decay,
                                                                                 time.time() - epoch_start_time))
            message = 'Epoch %d Losses: ' % epoch
            self.print_losses(message, self.loss_names(), mean_loss)
            
            if epoch % save_freq == 0:
                self.save_models(epoch)


class GANModel(BaseModel):
    
    def __init__(self, **kwargs):
        self.discriminator_layers = kwargs.get('num_layers_dis', 3)
        self.init_num_filters_dis = kwargs.get('init_filters_dis', 64)
        self.dis_model_type = kwargs.get('dis_model_type', 'patchgan')
        self.conv_activation_dis = kwargs.get('conv_activation_dis', 'relu')
        self.kernel_init_dis = kwargs.get('kernel_initializer_dis', 'he_normal')
        self.norm_layer_dis = kwargs.get('norm_method_dis', 'instance')
        self.discrim_iter = kwargs.get('discrim_iter', 1)
        self.gan_type = kwargs.get('gan_type', 'DCGAN')
        self.fuzzy_labels = kwargs.get('fuzzy_labels', False)
        
        self.pretrain_iters = kwargs.get('pretrain_iter', 0)
        self.skip_first_pretrain = kwargs.get('skip_first_pretrain', False)
        self.pool_size = kwargs.get('pool_size', 50)
        self.continue_gen_only = kwargs.get('continue_gen_only', False)
        
        self.dis_final_activation = 'sigmoid' if self.gan_type in ['DCGAN'] else 'linear'
        
        if self.dis_model_type == 'imagegan':
            self.discriminator_layers = int(math.log(kwargs.get('patch_size', (128, 128))[0], 2))

        super(GANModel, self).__init__(**kwargs)
        
    def load_models(self, model_dir, exp_to_load, which_epoch):
        if which_epoch == 'latest':
            g_a_models = sorted(glob(os.path.join(model_dir, exp_to_load + '_checkpoints', exp_to_load + '_' +
                                                  self.base_models[list(self.base_models.keys()[0])].name +
                                                  '_epoch*.h5')))
            epoch_number = int(str(os.path.basename(g_a_models[-1]).split('_')[-1]).split('.')[0].replace('epoch', ''))
        else:
            epoch_number = int(which_epoch)
        for model_arg, model_spec in self.base_models.items():
            if self.continue_gen_only and model_spec.model_type == 'dis':
                self.build_base_model(model_arg, model_spec)
            else:
                model_file = os.path.join(model_dir, exp_to_load + '_checkpoints',
                                          '%s_%s_epoch%03d.h5' % (exp_to_load, model_spec.name, epoch_number))
                self.print_message('Loading ' + model_spec.name + ' from file: ' + model_file)
                setattr(self, model_arg, load_model(model_file, custom_objects=SYNMI_CUSTOM_OBJECTS))
                getattr(self, model_arg).name = model_spec.name
        
    def build_base_model(self, model_arg, model_spec, for_summary=False):
        if model_spec.model_type == 'dis':
            input_nc = [getattr(self, selection) for selection in model_spec.inout][0]
            input_patch_size = (self.patch_size if for_summary else ((None,) * (2 if self.patch2D else 3))) \
                + (input_nc,)
            return build_discriminator_model(self.patch2D, input_patch_size, self.init_num_filters_dis,
                                             self.dis_model_type, self.discriminator_layers, self.conv_activation_dis,
                                             self.norm_layer_dis, self.kernel_init_dis, self.dis_final_activation)
        else:
            return super(GANModel, self).build_base_model(model_arg, model_spec, for_summary)
    
    # Borrowed from keras-contrib/examples/improved_wgan.py
    @staticmethod
    def wasserstein_loss(y_true, y_pred):
        """Calculates the Wasserstein loss for a sample batch.
            The Wasserstein loss function is very simple to calculate. In a standard GAN, the discriminator
            has a sigmoid output, representing the probability that samples are real or generated. In Wasserstein
            GANs, however, the output is linear with no activation function! Instead of being constrained to [0, 1],
            the discriminator wants to make the distance between its output for real and generated samples as large as
            possible. The most natural way to achieve this is to label generated samples -1 and real samples 1, instead
            of the 0 and 1 used in normal GANs, so that multiplying the outputs by the labels will give you the loss
            immediately. Note that the nature of this loss means that it can be (and frequently will be) less than 0."""
        return backend.mean(y_true * y_pred)
    
    def generate_labels(self, batch_size, output_shape):
        if self.gan_type in ['WGAN-GP', 'WGAN-LP', 'WGAN']:
            real_labels = np.random.uniform(-1.1, -0.9, (batch_size,) + output_shape) if self.fuzzy_labels \
                else (-1 * np.ones((batch_size,) + output_shape))
            fake_labels = np.random.uniform(0.9, 1.1, (batch_size,) + output_shape) \
                if self.fuzzy_labels else (np.ones((batch_size,) + output_shape))
            if self.gan_type in ['WGAN-GP', 'WGAN-LP']:
                dummy_labels = np.zeros((batch_size, 1))
                return real_labels, fake_labels, dummy_labels
            else:
                return real_labels, fake_labels
        else:
            real_labels = np.random.uniform(0.9, 1.1, (batch_size,) + output_shape) if self.fuzzy_labels \
                else (np.ones((batch_size,) + output_shape))
            fake_labels = np.random.uniform(0.0, 0.2, (batch_size,) + output_shape) \
                if self.fuzzy_labels else (np.zeros((batch_size,) + output_shape))
            return real_labels, fake_labels
    
    @staticmethod
    def clip_dis_weights(dis):
        for l in dis.layers:
            weights = l.get_weights()
            weights = [np.clip(w, -0.01, 0.01) for w in weights]
            l.set_weights(weights)
        
    def build_outer_models(self):
        raise NotImplementedError

    def fit(self, batch_size, n_epochs, n_epochs_decay, steps_per_epoch, save_freq, print_freq, starting_epoch):
        raise NotImplementedError
    

class Pix2Pix(GANModel):
    
    def __init__(self, **kwargs):
        self.input_a = None
        self.input_b = None
        self.gen = None
        self.dis = None
        self.generative_model = None
        self.adversarial_model = None

        self.dis_input_nc = kwargs.get('output_nc', 3) * 2

        lambda_a = kwargs.get('lambda_a', 10.0)
        lambda_gp = kwargs.get('lambda_gp', 10.0)
        generator_loss = kwargs.get('generator_loss', 'MAE')
        gan_type = kwargs.get('gan_type', 'DCGAN')
        
        gan_training_loss = ['binary_crossentropy'] * 2
        gan_testing_loss = 'binary_crossentropy'
        gan_loss_weights = [1.0] * 2
        gan_loss_names = ['D_Total', 'D_Real', 'D_Fake']
        if gan_type in ['LSGAN']:
            gan_training_loss = ['MSE'] * 2
            gan_testing_loss = 'MSE'
        elif gan_type in ['WGAN-GP', 'WGAN-LP']:
            gan_training_loss = [self.wasserstein_loss, self.wasserstein_loss, 'MSE']
            gan_testing_loss = self.wasserstein_loss
            gan_loss_weights = [1.0, 1.0, lambda_gp]
            gan_loss_names = ['D_Total', 'D_Real', 'D_Fake', 'D_GP']
        elif gan_type in ['WGAN']:
            gan_training_loss = [self.wasserstein_loss] * 2
            gan_testing_loss = self.wasserstein_loss
        
        self.inputs = ['input_a', 'input_b']
        self.base_models = OrderedDict([('gen', BaseModelSpec('G', 'gen', ('input_nc', 'output_nc'))),
                                        ('dis', BaseModelSpec('D', 'dis', ('dis_input_nc',)))])
        self.outer_models = OrderedDict([('adversarial_model', OuterModelSpec(gan_training_loss, gan_loss_weights,
                                                                              gan_loss_names)),
                                         ('generative_model', OuterModelSpec([gan_testing_loss, generator_loss],
                                                                             [1.0, lambda_a],
                                                                             ['G_Total', 'G_GAN', 'G_L1']))])
        
        super(Pix2Pix, self).__init__(**kwargs)
        
    def build_outer_models(self):
        # Build adversarial model
        with tf.device(self.merge_device):
            real_a = Input(shape=self.input_patch_size)
            real_b = Input(shape=self.output_patch_size)
            fake_b = Input(shape=self.output_patch_size)
            
            real_ab = Concatenate(axis=get_channel_axis())([real_a, real_b])
            fake_ab = Concatenate(axis=get_channel_axis())([real_a, fake_b])
        
            dis_real_ab = self.dis(real_ab)
            dis_fake_ab = self.dis(fake_ab)
            
            if self.gan_type in ['WGAN-GP', 'WGAN-LP']:
                grad_norm_cls = GradNormLP if self.gan_type == 'WGAN-LP' else GradNorm
                
                averaged_samples = Input(shape=get_input_shape(self.patch2D, self.dis_input_nc))
                sample_grad_norm = grad_norm_cls()([self.dis(averaged_samples), averaged_samples])
                self.adversarial_model = Model([real_a, real_b, fake_b, averaged_samples],
                                               [dis_real_ab, dis_fake_ab, sample_grad_norm])
            else:
                self.adversarial_model = Model([real_a, real_b, fake_b], [dis_real_ab, dis_fake_ab])
        self.compile('adversarial_model')
        
        # Freeze discriminator
        with tf.device(self.merge_device):
            self.make_trainable(self.dis, False)
        
        # Build generative model
        with tf.device(self.merge_device):
            real_a = Input(shape=self.input_patch_size)  # A
            fake_b = self.gen(real_a)  # B' = G_A(A)
            fake_ab = Concatenate(axis=get_channel_axis())([real_a, fake_b])
            dis_fake_ab = self.dis(fake_ab)
        
            self.generative_model = Model([real_a], [dis_fake_ab, fake_b])
        self.compile('generative_model')

    def fit(self, batch_size, n_epochs, n_epochs_decay, steps_per_epoch, save_freq,
            print_freq, starting_epoch):
    
        self.print_message('================ Training Loss (%s) ================' % time.strftime('%c'))

        batch_size = batch_size * self.num_gpus
        steps_per_epoch = steps_per_epoch // self.num_gpus
        
        batch_input_ph = Input(shape=self.patch_size + (self.input_nc, ))
        batch_output_ph = Input(shape=self.patch_size + (self.output_nc, ))
        dis_output_size = tuple(self.dis(Concatenate(axis=get_channel_axis())([batch_input_ph,
                                                                               batch_output_ph])).shape[1:])
    
        total_steps = 0
        for epoch in range(starting_epoch, n_epochs + n_epochs_decay + 1):
            epoch_start_time = time.time()
            if epoch > n_epochs:
                self.decay_learning_rate(epoch - n_epochs, n_epochs_decay)

            if self.pretrain_iters > 0 and (epoch > 1 or not self.skip_first_pretrain):
                self.print_message('Beginning Pre-Training of Discriminators for Epoch %d...' % epoch)

                pretrain_start_time = time.time()
                iter_losses = []
                losses = []
                for step in range(1, self.pretrain_iters + 1):
                    iter_start_time = time.time()
                    real_a = self.input_a(batch_size)
                    real_b = self.input_b(batch_size)
                    fake_b = self.gen.predict(real_a)
        
                    dis_labels = list(self.generate_labels(batch_size, dis_output_size))
        
                    if self.gan_type in ['WGAN-GP', 'WGAN-LP']:
                        epsilon = np.random.uniform(0, 1, size=(batch_size,) + (1,) * (len(real_a.shape) - 1))
                        averaged_samples = np.concatenate([real_a, epsilon * real_b + (1 - epsilon) * fake_b],
                                                          axis=get_channel_axis())
                        dis_inputs = [real_a, real_b, fake_b, averaged_samples]
                    else:
                        dis_inputs = [real_a, real_b, fake_b]
        
                    d_loss = self.adversarial_model.train_on_batch(dis_inputs, dis_labels)
        
                    if self.gan_type in ['WGAN']:
                        self.clip_dis_weights(self.dis)
        
                    iter_losses.append(d_loss)
        
                    if step % print_freq == 0:
                        mean_iter_loss = []
                        for loss in zip(*iter_losses):
                            mean_iter_loss.append(np.mean(loss))
                        losses.extend(iter_losses)
                        iter_losses = []
                        time_per_img = (time.time() - iter_start_time) / batch_size
                        message = '(pre-train, epoch: %03d, iter: %04d, time: %.3f) ' % (epoch, step, time_per_img)
                        loss_names = self.loss_names()[-4:] if self.gan_type in ['WGAN-GP', 'WGAN-LP'] \
                            else self.loss_names()[-3:]
                        self.print_losses(message, loss_names, mean_iter_loss)

                self.print_message('End of pre-training %d \t Time Elapsed: %d sec' %
                                   (epoch, time.time() - pretrain_start_time))

                mean_loss = []
                for loss in zip(*losses):
                    mean_loss.append(np.mean(loss))
                message = 'Discriminator Pre-Training %d Losses: ' % epoch
                loss_names = self.loss_names()[-4:] if self.gan_type in ['WGAN-GP', 'WGAN-LP'] \
                    else self.loss_names()[-3:]
                self.print_losses(message, loss_names, mean_loss)

            self.print_message('Beginning GAN Training for Epoch %d...' % epoch)
            iter_losses = []
            losses = []
            for i in range(steps_per_epoch):
                iter_start_time = time.time()
                total_steps += 1
                
                d_loss_iter = []
                real_a = None
                real_b = None
                for j in range(min(self.discrim_iter, 1)):
                    real_a = self.input_a(batch_size)
                    real_b = self.input_b(batch_size)
                    fake_b = self.gen.predict(real_a)
                    
                    dis_labels = list(self.generate_labels(batch_size, dis_output_size))
                    
                    if self.gan_type in ['WGAN-GP', 'WGAN-LP']:
                        epsilon = np.random.uniform(0, 1, size=(batch_size,) + (1,) * (len(real_a.shape) - 1))
                        averaged_samples = np.concatenate([real_a, epsilon * real_b + (1 - epsilon) * fake_b],
                                                          axis=get_channel_axis())
                        dis_inputs = [real_a, real_b, fake_b, averaged_samples]
                    else:
                        dis_inputs = [real_a, real_b, fake_b]
                
                    d_loss = self.adversarial_model.train_on_batch(dis_inputs, dis_labels)

                    if self.gan_type in ['WGAN']:
                        self.clip_dis_weights(self.dis)
                    
                    d_loss_iter.append(d_loss)
                d_loss = [np.mean(losses) for losses in list(zip(*d_loss_iter))]
                
                real_labels = self.generate_labels(batch_size, dis_output_size)[0]
            
                g_loss = self.generative_model.train_on_batch([real_a], [real_labels, real_b])
            
                iter_losses.append(d_loss + g_loss)
            
                if total_steps % print_freq == 0:
                    mean_iter_loss = []
                    for loss in zip(*iter_losses):
                        mean_iter_loss.append(np.mean(loss))
                    losses.extend(iter_losses)
                    iter_losses = []
                    time_per_img = (time.time() - iter_start_time) / batch_size
                    message = '(epoch: %d, iters: %d, time: %.3f) ' % (epoch, i + 1, time_per_img)
                    self.print_losses(message, self.loss_names(), mean_iter_loss)
                
                    fake_b = self.gen.predict(real_a)
                    visuals = OrderedDict([('real_A', real_a[0, ...]), ('fake_B', fake_b[0, ...]),
                                           ('real_B', real_b[0, ...])])
                    save_training_page(self.model_dir, self.experiment_name, visuals, epoch)
        
            losses.extend(iter_losses)
            mean_loss = []
            for loss in zip(*losses):
                mean_loss.append(np.mean(loss))
        
            self.print_message('End of epoch %d / %d \t Time Elapsed: %d sec' % (epoch, n_epochs + n_epochs_decay,
                                                                                 time.time() - epoch_start_time))
            message = 'Epoch %d Losses: ' % epoch
            self.print_losses(message, self.loss_names(), mean_loss)
        
            if epoch % save_freq == 0:
                self.save_models(epoch)


class CycleGAN(GANModel):

    def __init__(self, **kwargs):
        self.input_a = None
        self.input_b = None
        self.gen_a = None
        self.gen_b = None
        self.dis_a = None
        self.dis_b = None
        self.generative_model = None
        self.discriminator_model_a = None
        self.discriminator_model_b = None

        self.use_identity_loss = kwargs.get('use_identity_loss', False)
        lambda_a = kwargs.get('lambda_a', 10.0)
        lambda_b = kwargs.get('lambda_b', 10.0)
        lambda_gp = kwargs.get('lambda_gp', 10.0)
        lambda_id = kwargs.get('lambda_id', 1.0)
        generator_loss = kwargs.get('generator_loss', 'MAE')
        gan_type = kwargs.get('gan_type', 'DCGAN')

        dis_training_loss = ['binary_crossentropy'] * 2
        dis_testing_loss = 'binary_crossentropy'
        dis_loss_weights = [1.0] * 2
        dis_loss_names_a = ['Total_A', 'Real_A', 'Fake_A']
        dis_loss_names_b = ['Total_B', 'Real_B', 'Fake_B']
        if gan_type in ['LSGAN']:
            dis_training_loss = ['MSE'] * 2
            dis_testing_loss = 'MSE'
        elif gan_type in ['WGAN-GP', 'WGAN-LP']:
            dis_training_loss = [self.wasserstein_loss, self.wasserstein_loss, 'MSE']
            dis_testing_loss = self.wasserstein_loss
            dis_loss_weights = [1.0, 1.0, lambda_gp]
            dis_loss_names_a = ['Total_A', 'Real_A', 'Fake_A', 'GP_A']
            dis_loss_names_b = ['Total_B', 'Real_B', 'Fake_B', 'GP_B']
        elif gan_type in ['WGAN']:
            dis_training_loss = [self.wasserstein_loss] * 2
            dis_testing_loss = self.wasserstein_loss
        
        if self.use_identity_loss and kwargs.get('input_nc') != kwargs.get('output_nc'):
            raise ValueError('Identity mapping is not supported with unequal channels between inputs and outputs.')

        if self.use_identity_loss:
            gen_loss_weights = [1.0, 1.0, lambda_a, lambda_b, lambda_id * lambda_a, lambda_id * lambda_b]
            gen_loss_functions = [dis_testing_loss, dis_testing_loss, generator_loss, generator_loss,
                                  generator_loss, generator_loss]
            gen_loss_names = ['G_Total', 'D_A', 'D_B', 'Cyc_A', 'Cyc_B', 'Id_A', 'Id_B']
        else:
            gen_loss_weights = [1.0, 1.0, lambda_a, lambda_b]
            gen_loss_functions = [dis_testing_loss, dis_testing_loss, generator_loss, generator_loss]
            gen_loss_names = ['G_Total', 'D_A', 'D_B', 'Cyc_A', 'Cyc_B']

        self.inputs = ['input_a', 'input_b']
        self.base_models = OrderedDict([('gen_a', BaseModelSpec('G_A', 'gen', ('input_nc', 'output_nc'))),
                                        ('gen_b', BaseModelSpec('G_B', 'gen', ('output_nc', 'input_nc'))),
                                        ('dis_a', BaseModelSpec('D_A', 'dis', ('input_nc',))),
                                        ('dis_b', BaseModelSpec('D_B', 'dis', ('output_nc',)))])
        self.outer_models = OrderedDict([('generative_model', OuterModelSpec(gen_loss_functions, gen_loss_weights,
                                                                             gen_loss_names)),
                                         ('discriminator_model_a', OuterModelSpec(dis_training_loss, dis_loss_weights,
                                                                                  dis_loss_names_a)),
                                         ('discriminator_model_b', OuterModelSpec(dis_training_loss, dis_loss_weights,
                                                                                  dis_loss_names_b))])

        super(CycleGAN, self).__init__(**kwargs)
        
    def build_outer_models(self):
        # Build adversarial models
        with tf.device(self.merge_device):
            real_a = Input(shape=self.input_patch_size, name='real_a')
            fake_a = Input(shape=self.input_patch_size, name='fake_a')
            real_b = Input(shape=self.output_patch_size, name='real_b')
            fake_b = Input(shape=self.output_patch_size, name='fake_b')
        
            dis_real_a = self.dis_a(real_a)
            dis_fake_a = self.dis_a(fake_a)
            dis_real_b = self.dis_b(real_b)
            dis_fake_b = self.dis_b(fake_b)
            
            if self.gan_type in ['WGAN-GP', 'WGAN-LP']:
                grad_norm_cls = GradNormLP if self.gan_type == 'WGAN-LP' else GradNorm
                
                averaged_samples_a = Input(self.input_patch_size)
                averaged_samples_b = Input(self.output_patch_size)
                sample_grad_norm_a = grad_norm_cls()([self.dis_a(averaged_samples_a), averaged_samples_a])
                sample_grad_norm_b = grad_norm_cls()([self.dis_b(averaged_samples_b), averaged_samples_b])
                self.discriminator_model_a = Model([real_a, fake_a, averaged_samples_a],
                                                   [dis_real_a, dis_fake_a, sample_grad_norm_a])
                self.discriminator_model_b = Model([real_b, fake_b, averaged_samples_b],
                                                   [dis_real_b, dis_fake_b, sample_grad_norm_b])
            else:
                self.discriminator_model_a = Model([real_a, fake_a], [dis_real_a, dis_fake_a])
                self.discriminator_model_b = Model([real_b, fake_b], [dis_real_b, dis_fake_b])
        self.compile('discriminator_model_a')
        self.compile('discriminator_model_b')
        
        # Freeze discriminators
        with tf.device(self.merge_device):
            self.make_trainable(self.dis_a, False)
            self.make_trainable(self.dis_b, False)
        
        # TODO: Change generator names to reflect what they create?
        # Build generative model
        with tf.device(self.merge_device):
            # real_a = Input(shape=self.input_patch_size)  # A
            fake_b = self.gen_a(real_a)  # B' = G_A(A)
            recon_a = self.gen_b(fake_b)  # A'' = G_B(G_A(A))
            dis_fake_b = self.dis_b(fake_b)  # D_A(G_A(A))
        
            # real_b = Input(shape=self.output_patch_size)  # B
            fake_a = self.gen_b(real_b)  # A' = G_B(B)
            recon_b = self.gen_a(fake_a)  # B'' = G_A(G_B(B))
            dis_fake_a = self.dis_a(fake_a)  # D_B(G_B(B))
        
            if self.use_identity_loss:
                id_a = self.gen_b(real_a)  # I' = G_B(A)
                id_b = self.gen_a(real_b)  # I' = G_A(B)
                self.generative_model = Model([real_a, real_b], [dis_fake_a, dis_fake_b, recon_a, recon_b, id_a, id_b])
            else:
                self.generative_model = Model([real_a, real_b], [dis_fake_a, dis_fake_b, recon_a, recon_b])
        self.compile('generative_model')
                    
    def fit(self, batch_size, n_epochs, n_epochs_decay, steps_per_epoch, save_freq,
            print_freq, starting_epoch):
    
        self.print_message('================ Training Loss (%s) ================' % time.strftime('%c'))

        batch_size = batch_size * self.num_gpus
        steps_per_epoch = steps_per_epoch // self.num_gpus

        batch_input_ph = Input(shape=self.patch_size + (self.input_nc,))
        batch_output_ph = Input(shape=self.patch_size + (self.output_nc,))
        dis_a_output_size = tuple(self.dis_a(batch_input_ph).shape[1:])
        dis_b_output_size = tuple(self.dis_b(batch_output_ph).shape[1:])
            
        total_steps = 0
        for epoch in range(starting_epoch, n_epochs + n_epochs_decay + 1):
            epoch_start_time = time.time()
            
            if epoch > n_epochs:
                self.decay_learning_rate(epoch - n_epochs, n_epochs_decay)

            if self.pretrain_iters > 0 and (epoch > 1 or not self.skip_first_pretrain):
                self.print_message('Beginning Pre-Training of Discriminators for Epoch %d...' % epoch)
                
                pretrain_start_time = time.time()
                iter_losses = []
                losses = []
                for step in range(1, self.pretrain_iters + 1):
                    iter_start_time = time.time()
        
                    real_a = self.input_a(batch_size)
                    real_b = self.input_b(batch_size)
                    fake_a = self.gen_b.predict(real_b)
                    fake_b = self.gen_a.predict(real_a)
        
                    dis_labels_a = list(self.generate_labels(batch_size, dis_a_output_size))
                    dis_labels_b = list(self.generate_labels(batch_size, dis_b_output_size))
        
                    if self.gan_type in ['WGAN-GP', 'WGAN-LP']:
                        epsilon = np.random.uniform(0, 1, size=(batch_size,) + (1,) * (len(real_a.shape) - 1))
                        averaged_samples_a = epsilon * real_a + (1 - epsilon) * fake_a
                        dis_inputs_a = [real_a, fake_a, averaged_samples_a]
                        averaged_samples_b = epsilon * real_b + (1 - epsilon) * fake_b
                        dis_inputs_b = [real_b, fake_b, averaged_samples_b]
                    else:
                        dis_inputs_a = [real_a, fake_a]
                        dis_inputs_b = [real_b, fake_b]
        
                    d_a_loss = self.discriminator_model_a.train_on_batch(dis_inputs_a, dis_labels_a)
                    d_b_loss = self.discriminator_model_b.train_on_batch(dis_inputs_b, dis_labels_b)
        
                    if self.gan_type in ['WGAN']:
                        self.clip_dis_weights(self.dis_a)
                        self.clip_dis_weights(self.dis_b)
        
                    iter_losses.append(d_a_loss + d_b_loss)
        
                    if step % print_freq == 0:
                        mean_iter_loss = []
                        for loss in zip(*iter_losses):
                            mean_iter_loss.append(np.mean(loss))
                        losses.extend(iter_losses)
                        iter_losses = []
                        time_per_img = (time.time() - iter_start_time) / batch_size
                        message = '(pre-train, epoch: %03d, iter: %04d, time: %.3f) ' % (epoch, step, time_per_img)
                        loss_names = self.loss_names()[-8:] if self.gan_type in ['WGAN-GP', 'WGAN-LP'] \
                            else self.loss_names()[-6:]
                        self.print_losses(message, loss_names, mean_iter_loss)

                self.print_message('End of pre-training %d \t Time Elapsed: %d sec' %
                                   (epoch, time.time() - pretrain_start_time))
    
                mean_loss = []
                for loss in zip(*losses):
                    mean_loss.append(np.mean(loss))
                message = 'Discriminator Pre-Training %d Losses: ' % epoch
                loss_names = self.loss_names()[-8:] if self.gan_type in ['WGAN-GP', 'WGAN-LP'] \
                    else self.loss_names()[-6:]
                self.print_losses(message, loss_names, mean_loss)
            
            pool_a = ImagePool(self.pool_size)
            pool_b = ImagePool(self.pool_size, master=pool_a)

            self.print_message('Beginning GAN Training for Epoch %d...' % epoch)
            iter_losses = []
            losses = []
            for i in range(steps_per_epoch):
                iter_start_time = time.time()
                total_steps += 1

                real_a = self.input_a(batch_size)
                real_b = self.input_b(batch_size)
                fake_a = pool_a.add_and_generate(self.gen_b.predict(real_b))
                fake_b = pool_b.add_and_generate(self.gen_a.predict(real_a))

                real_labels_a = self.generate_labels(batch_size, dis_a_output_size)[0]
                real_labels_b = self.generate_labels(batch_size, dis_b_output_size)[0]
                
                if self.use_identity_loss:
                    g_loss = self.generative_model.train_on_batch([real_a, real_b], [real_labels_a, real_labels_b,
                                                                                     real_a, real_b,
                                                                                     real_a, real_b])
                else:
                    g_loss = self.generative_model.train_on_batch([real_a, real_b], [real_labels_a, real_labels_b,
                                                                                     real_a, real_b])

                # TODO: Look into how we are training CycleGAN (is everything ok?)
                d_loss_iter = []
                for j in range(self.discrim_iter):
                    dis_labels_a = list(self.generate_labels(batch_size, dis_a_output_size))
                    dis_labels_b = list(self.generate_labels(batch_size, dis_b_output_size))

                    if self.gan_type in ['WGAN-GP', 'WGAN-LP']:
                        epsilon = np.random.uniform(0, 1, size=(batch_size,) + (1,) * (len(real_a.shape) - 1))
                        averaged_samples_a = epsilon * real_a + (1 - epsilon) * fake_a
                        dis_inputs_a = [real_a, fake_a, averaged_samples_a]
                        averaged_samples_b = epsilon * real_b + (1 - epsilon) * fake_b
                        dis_inputs_b = [real_b, fake_b, averaged_samples_b]
                    else:
                        dis_inputs_a = [real_a, fake_a]
                        dis_inputs_b = [real_b, fake_b]
                    
                    d_a_loss = self.discriminator_model_a.train_on_batch(dis_inputs_a, dis_labels_a)
                    d_b_loss = self.discriminator_model_b.train_on_batch(dis_inputs_b, dis_labels_b)
                    
                    if self.gan_type in ['WGAN']:
                        self.clip_dis_weights(self.dis_a)
                        self.clip_dis_weights(self.dis_b)
                    
                    d_loss_iter.append(d_a_loss + d_b_loss)
                    
                    if j < (self.discrim_iter - 1):  # TODO: Specifically here? and should we train in this order?
                        real_a = self.input_a(batch_size)
                        real_b = self.input_b(batch_size)
                        fake_a = pool_a.generate_batch(batch_size)
                        fake_b = pool_b.generate_batch(batch_size)
                
                d_loss = [np.mean(losses) for losses in list(zip(*d_loss_iter))]
                
                iter_losses.append(g_loss + d_loss)
                
                if total_steps % print_freq == 0:
                    mean_iter_loss = []
                    for loss in zip(*iter_losses):
                        mean_iter_loss.append(np.mean(loss))
                    losses.extend(iter_losses)
                    iter_losses = []
                    time_per_img = (time.time() - iter_start_time) / batch_size
                    message = '(epoch: %03d, iters: %04d, time: %.3f) ' % (epoch, i+1, time_per_img)
                    self.print_losses(message, self.loss_names(), mean_iter_loss)

                    fake_a = self.gen_b.predict(real_b)
                    fake_b = self.gen_a.predict(real_a)
                    rec_a = self.gen_b.predict(fake_b)
                    rec_b = self.gen_a.predict(fake_a)
                    if self.use_identity_loss:
                        id_a = self.gen_b.predict(real_a)
                        id_b = self.gen_a.predict(real_b)
                        visuals = OrderedDict([('real_A', real_a[0, ...]), ('fake_B', fake_b[0, ...]),
                                               ('rec_A', rec_a[0, ...]), ('idt_A', id_a[0, ...]),
                                               ('real_B', real_b[0, ...]), ('fake_A', fake_a[0, ...]),
                                               ('rec_B', rec_b[0, ...]), ('idt_b', id_b[0, ...])])
                    else:
                        visuals = OrderedDict([('real_A', real_a[0, ...]), ('fake_B', fake_b[0, ...]),
                                               ('rec_A', rec_a[0, ...]), ('real_B', real_b[0, ...]),
                                               ('fake_A', fake_a[0, ...]), ('rec_B', rec_b[0, ...])])
                    save_training_page(self.model_dir, self.experiment_name, visuals, epoch)
            
            losses.extend(iter_losses)
            mean_loss = []
            for loss in zip(*losses):
                mean_loss.append(np.mean(loss))
            
            self.print_message('End of epoch %d / %d \t Time Elapsed: %d sec' % (epoch, n_epochs + n_epochs_decay,
                                                                                 time.time() - epoch_start_time))
            message = 'Epoch %d Losses: ' % epoch
            self.print_losses(message, self.loss_names(), mean_loss)
            
            if epoch % save_freq == 0:
                self.save_models(epoch)


class PredictionModel(object):
    def __init__(self, model_file):
        self.model = load_model(model_file, custom_objects=SYNMI_CUSTOM_OBJECTS)
        self.model.summary()
    
    def predict(self, input_generator, output_sink, batch_size):
        result_images = self.model.predict(input_generator.patches, batch_size=batch_size, verbose=True)
        output_sink(result_images)


class ImagePool(object):
    
    def __init__(self, pool_size, master=None):
        self.pool_size = pool_size
        self.master = master
        self.current_indexes = None
        self.images = []
        
    def add_to_pool(self, image_list):
        self.images.extend(image_list)
        self.images = self.images[-self.pool_size:]
    
    def add_and_generate(self, img_list):
        batch = []
        if self.master is None:
            self.current_indexes = []
            switches = np.random.randint(0, 2, len(img_list)).astype(bool)
            for i, img in enumerate(img_list):
                if len(self.images) < self.pool_size:
                    self.images.append(img)
                    batch.append(img)
                    self.current_indexes.append(-1)
                else:
                    if switches[i]:
                        index = np.random.randint(0, len(self.images))
                        batch.append(self.images[index])
                        self.images[index] = img
                        self.current_indexes.append(index)
                    else:
                        batch.append(img)
                        self.current_indexes.append(None)
        else:
            self.current_indexes = self.master.current_indexes
            for i, img in enumerate(img_list):
                if self.current_indexes[i] is None:
                    batch.append(img)
                elif self.current_indexes[i] >= 0:
                    batch.append(self.images[self.current_indexes[i]])
                    self.images[self.current_indexes[i]] = img
                else:
                    self.images.append(img)
                    batch.append(img)
        return np.array(batch)

    def generate_batch(self, batch_size):
        self.current_indexes = np.random.choice(range(len(self.images)), batch_size, replace=False) \
            if self.master is None else self.master.current_indexes
        batch = []
        for idx in self.current_indexes:
            batch.append(self.images[idx])
        return np.array(batch)
