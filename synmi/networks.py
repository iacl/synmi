from __future__ import print_function, division

from keras.models import Model, Input
from keras.layers import (Conv2D, Conv3D, Conv2DTranspose, Conv3DTranspose, BatchNormalization, LeakyReLU, Concatenate,
                          Activation, Add, UpSampling2D, UpSampling3D, MaxPooling2D, MaxPooling3D, AveragePooling2D,
                          AveragePooling3D)
from keras.initializers import RandomNormal


from .utils import get_channel_axis, get_image_data_format
from .layers import BatchNormalizationGAN, ReflectionPadding2D, ReflectionPadding3D


def build_generator_model(patch2d, input_patch_size, output_nc, init_num_filters=64, model_name='resnet', n_levels=6,
                          n_blocks=2, downsample_method='max_pool', upsample_method='upsample', z_res_change='all',
                          conv_activation='relu', kernel_init='he_normal', padding='zero', norm_layer='instance',
                          skip_connections='all', resblocks=False, gen_final_activation='relu'):
    if model_name == 'unet':
        gen_model = build_unet(patch2d, input_patch_size, output_nc, init_num_filters, n_levels=n_levels,
                               n_blocks=n_blocks, downsample_method=downsample_method, upsample_method=upsample_method,
                               z_res_change=z_res_change, conv_activation=conv_activation, kernel_init=kernel_init,
                               padding=padding, norm_layer=norm_layer, skip_connections=skip_connections,
                               gen_final_activation=gen_final_activation, resblocks=resblocks)
    elif model_name == 'resnet':
        gen_model = build_resnet(patch2d, input_patch_size, output_nc, init_num_filters, n_levels=n_levels,
                                 n_blocks=n_blocks, downsample_method=downsample_method,
                                 upsample_method=upsample_method, z_res_change=z_res_change,
                                 conv_activation=conv_activation, kernel_init=kernel_init, padding=padding,
                                 norm_layer=norm_layer, gen_final_activation=gen_final_activation)
    else:
        raise NotImplementedError('Generator model name [%s] is not recognized' % model_name)
    
    return gen_model


def build_discriminator_model(patch2d, input_patch_size, init_num_filters, model_name='patchgan', n_layers=3,
                              conv_activation='relu', norm_layer='instance', kernel_init='he_normal',
                              dis_final_activation='sigmoid'):
    if model_name == 'patchgan':
        dis_model = build_nlayer_discriminator(patch2d, input_patch_size, init_num_filters, n_layers, conv_activation,
                                               norm_layer, kernel_init, dis_final_activation)
    elif model_name == 'imagegan':
        dis_model = build_image_discriminator(patch2d, input_patch_size, init_num_filters, n_layers, conv_activation,
                                              norm_layer, kernel_init, dis_final_activation)
    else:
        raise NotImplementedError('Discriminator model name [%s] is not recognized' % model_name)
    
    return dis_model


def build_unet(patch2d, input_patch_size, output_nc, init_num_filters, n_levels=4, n_blocks=2, norm_layer='instance',
               downsample_method='max_pool', upsample_method='upsample', z_res_change='all', skip_connections='all',
               kernel_init='he_normal', padding='zero', conv_activation='relu', gen_final_activation=None,
               resblocks=False):
    use_bias = norm_layer == 'instance'
    
    input_img = Input(shape=input_patch_size, name='input1')
    
    patch_len = 2 if patch2d else 3
    
    base_kernel = (3,) * patch_len
    change_kernel = (4,) * patch_len
    base_stride = (1,) * patch_len
    change_stride = (2,) * patch_len
    
    convs = []
    norm = input_img
    for i in range(n_levels):
        conv = norm
        for j in range(n_blocks):
            conv = conv_block(conv, patch2d, init_num_filters * (2 ** i), base_kernel, base_stride, padding,
                              conv_activation, norm_layer, use_bias, kernel_init)
        conv = Add()([norm, conv]) if resblocks else conv
        convs.append(conv)
        downsample_z = (change_stride[:2] + (1,)) \
            if (not patch2d) and (z_res_change == 'none' or (z_res_change == 'half' and i % 2 != 0)) \
            else change_stride
        norm = downsample_block(conv, patch2d, downsample_method, init_num_filters * (2 ** i), change_kernel,
                                downsample_z, conv_activation, norm_layer, use_bias, kernel_init)
    
    bottom = norm
    for j in range(n_blocks):
        bottom = conv_block(bottom, patch2d, init_num_filters * (2 ** n_levels), base_kernel, base_stride, padding,
                            conv_activation, norm_layer, use_bias, kernel_init)
    norm = Add()([norm, bottom]) if resblocks else bottom
    
    for i in reversed(range(n_levels)):
        upsample_z = (change_stride[:2] + (1,)) \
            if (not patch2d) and (z_res_change == 'none' or (z_res_change == 'half' and i % 2 != 0)) \
            else change_stride
        up = upsample_block(norm, patch2d, upsample_method, init_num_filters * (2 ** i), change_kernel, upsample_z,
                            conv_activation, norm_layer, use_bias, kernel_init)
        concat = Concatenate(axis=get_channel_axis())([up, convs[i]]) if skip_connections in ['all', 'unet'] else up
        norm = concat
        for j in range(n_blocks):
            norm = conv_block(norm, patch2d, init_num_filters * (2 ** i), base_kernel, base_stride, padding,
                              conv_activation, norm_layer, use_bias, kernel_init)
        norm = Add()([up, norm]) if resblocks else norm
    
    if skip_connections == 'all':
        norm = Concatenate(axis=get_channel_axis())([input_img, norm])
    conv = get_conv_class(patch2d)(output_nc, (1,) * patch_len, use_bias=use_bias,
                                   kernel_initializer=get_kernel_init(kernel_init))(norm)
    act = get_activation(gen_final_activation)(conv)
    
    return Model(inputs=input_img, outputs=act)


def build_resnet(patch2d, input_patch_size, output_nc, init_num_filters, n_levels=2, n_blocks=6, norm_layer='instance',
                 downsample_method='max_pool', upsample_method='upsample', z_res_change='all', conv_activation='relu',
                 kernel_init='he_normal', padding='zero', gen_final_activation=None):
    patch_len = 2 if patch2d else 3
    
    outer_kernel_size = (7,) * patch_len
    kernel_size = (4,) * patch_len
    bottleneck_kernel_size = (3,) * patch_len
    base_stride = (1,) * patch_len
    change_stride = (2,) * patch_len
    
    use_bias = norm_layer == 'instance'
    
    input_img = Input(shape=input_patch_size, name='input1')
    norm = conv_block(input_img, patch2d, init_num_filters, outer_kernel_size, base_stride, padding, conv_activation,
                      norm_layer, use_bias, kernel_init)
    
    for i in range(n_levels):
        downsample_z = (change_stride[:2] + (1,)) \
            if (not patch2d) and (z_res_change == 'none' or (z_res_change == 'half' and i % 2 != 0)) \
            else change_stride
        norm = downsample_block(norm, patch2d, downsample_method, init_num_filters * (2 ** (i + 1)), kernel_size,
                                downsample_z, conv_activation, norm_layer, use_bias, kernel_init)
        if downsample_method in ['max_pool', 'avg_pool']:
            norm = conv_block(norm, patch2d, init_num_filters * (2 ** (i + 1)), kernel_size, base_stride, 'same',
                              conv_activation, norm_layer, use_bias, kernel_init)
    
    for i in range(n_blocks):
        conv = conv_block(norm, patch2d, init_num_filters * (2 ** n_levels), bottleneck_kernel_size, base_stride,
                          padding, conv_activation, norm_layer, use_bias, kernel_init)
        conv = conv_block(conv, patch2d, init_num_filters * (2 ** n_levels), bottleneck_kernel_size, base_stride,
                          padding, 'linear', norm_layer, use_bias, kernel_init)
        norm = Add()([norm, conv])
    
    for i in reversed(range(n_levels)):
        upsample_z = (change_stride[:2] + (1,)) \
            if (not patch2d) and (z_res_change == 'none' or (z_res_change == 'half' and i % 2 != 0)) \
            else change_stride
        norm = upsample_block(norm, patch2d, upsample_method, init_num_filters * (2 ** i), kernel_size, upsample_z,
                              conv_activation, norm_layer, use_bias, kernel_init)
        if upsample_method == 'upsample':
            norm = conv_block(norm, patch2d, init_num_filters * (2 ** (i + 1)), kernel_size, base_stride, 'same',
                              conv_activation, norm_layer, use_bias, kernel_init)
    
    conv = get_conv_class(patch2d)(output_nc, outer_kernel_size, padding='same', use_bias=use_bias,
                                   kernel_initializer=get_kernel_init(kernel_init))(norm)
    act = get_activation(gen_final_activation)(conv)
    
    return Model(inputs=input_img, outputs=act)


def build_image_discriminator(patch2d, input_patch_size, init_num_filters=64, n_layers=6, conv_activation='relu',
                              norm_layer='instance', kernel_init='he_normal', dis_final_activation='sigmoid'):
    patch_len = 2 if patch2d else 3
    
    kernel_size = (4,) * patch_len
    change_stride = (2,) * patch_len
    
    use_bias = norm_layer == 'instance'
    
    input_img = Input(shape=input_patch_size, name='input1')
    conv = input_img
    for n in range(n_layers):
        conv = conv_block(conv, patch2d, init_num_filters * min(2 ** n, 8) if n < (n_layers - 1) else 1, kernel_size,
                          change_stride, 'same', conv_activation if n < (n_layers - 1) else dis_final_activation,
                          norm_layer if n < (n_layers - 1) else None, use_bias, kernel_init)
    return Model(inputs=input_img, outputs=conv)


def build_nlayer_discriminator(patch2d, input_patch_size, init_num_filters=64, n_layers=3, conv_activation='relu',
                               norm_layer='instance', kernel_init='he_normal', dis_final_activation='sigmoid'):
    patch_len = 2 if patch2d else 3
    
    kernel_size = (4,) * patch_len
    base_stride = (1,) * patch_len
    change_stride = (2,) * patch_len
    
    use_bias = norm_layer == 'instance'
    
    input_img = Input(shape=input_patch_size, name='input1')
    conv = input_img
    for n in range(n_layers):
        conv = conv_block(conv, patch2d, init_num_filters * min(2 ** n, 8), kernel_size, change_stride, 'same',
                          conv_activation, norm_layer if n > 0 else None, use_bias, kernel_init)
    conv = conv_block(conv, patch2d, init_num_filters * min(2 ** n_layers, 8), kernel_size, base_stride, 'same',
                      conv_activation, norm_layer, use_bias, kernel_init)
    conv = conv_block(conv, patch2d, 1, kernel_size, base_stride, 'same', dis_final_activation, None, use_bias,
                      kernel_init)
    return Model(inputs=input_img, outputs=conv)


def conv_block(previous, patch2d, num_filters, kernel_size, strides, padding, conv_activation, norm_layer,
               use_bias, kernel_init, conv_type='standard'):
    conv_obj = get_conv_class(patch2d, conv_type == 'transpose')
    if padding in ['zero', 'same']:
        pad = previous
        conv_padding = 'same'
    elif padding == 'reflect':
        padding_size = []
        for kern, stride in zip(kernel_size, strides):
            pad_along_side = max(kern - stride, 0)
            pad_first = pad_along_side // 2
            pad_second = pad_along_side - pad_first
            padding_size.append((pad_first, pad_second))
        pad_class = ReflectionPadding2D if patch2d else ReflectionPadding3D
        pad = pad_class(padding_size)(previous)
        conv_padding = 'valid'
    else:
        raise ValueError('Unknown padding type')
    
    conv = conv_obj(num_filters, kernel_size, padding=conv_padding, strides=strides, use_bias=use_bias,
                    kernel_initializer=get_kernel_init(kernel_init))(pad)
    act = get_activation(conv_activation)(conv)
    if norm_layer is not None:
        return get_norm_layer(norm_layer)(act)
    else:
        return act


def upsample_block(previous, patch2d, block_type, num_filters, kernel_size, strides, conv_activation,
                   norm_layer, use_bias, kernel_init):
    if block_type == 'deconv':
        return conv_block(previous, patch2d, num_filters, kernel_size, strides, 'same', conv_activation, norm_layer,
                          use_bias, kernel_init, 'transpose')
    elif block_type == 'upsample':
        pool_class = UpSampling2D if patch2d else UpSampling3D
        return pool_class(size=strides)(previous)
    else:
        return NotImplementedError('Upsampling block name [%s] is not recognized.' % block_type)
    
    
def downsample_block(previous, patch2d, block_type, num_filters, kernel_size, strides, conv_activation,
                     norm_layer, use_bias, kernel_init):
    if block_type == 'strided':
        return conv_block(previous, patch2d, num_filters, kernel_size, strides, 'same', conv_activation, norm_layer,
                          use_bias, kernel_init)
    elif block_type == 'max_pool':
        pool_class = MaxPooling2D if patch2d else MaxPooling3D
        return pool_class(pool_size=strides, data_format=get_image_data_format())(previous)
    elif block_type == 'avg_pool':
        pool_class = AveragePooling2D if patch2d else AveragePooling3D
        return pool_class(pool_size=strides, data_format=get_image_data_format())(previous)
    else:
        return NotImplementedError('Downsampling block name [%s] is not recognized.' % block_type)


def get_norm_layer(layer_name):
    if layer_name == 'batch-gan':
        return BatchNormalizationGAN(axis=get_channel_axis())
    elif layer_name == 'batch':
        return BatchNormalization(axis=get_channel_axis())
    elif layer_name == 'instance':
        try:
            from keras_contrib.layers import InstanceNormalization
        except ImportError:
            raise ImportError('keras_contrib is required to use InstanceNormalization layers. Install keras_contrib or '
                              'switch normalization to "batch".')
        return InstanceNormalization(axis=get_channel_axis())
    else:
        return NotImplementedError('Normalization layer name [%s] is not recognized.' % layer_name)


def get_activation(act_type, alpha=0.2):
    if act_type == 'normalized-relu':
        act_type = 'relu'
    return LeakyReLU(alpha) if act_type == 'leaky_relu' else Activation(act_type)


def get_conv_class(patch2d, transpose=False):
    if patch2d:
        return Conv2DTranspose if transpose else Conv2D
    else:
        return Conv3DTranspose if transpose else Conv3D


def get_kernel_init(kernel_init):
    return RandomNormal(mean=0.0, stddev=0.02) if kernel_init == 'random_normal' else kernel_init
