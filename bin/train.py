import os
import sys
import argparse
import datetime
import warnings

from synmi.models import CycleGAN, CycleNoGAN, Pix2Pix, SingleCovNet
from synmi.generators import NiftiGenerator
from synmi.utils import get_files_extension

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    required = parser.add_argument_group('required')
    required.add_argument('--sources', required=True, nargs='+',
                          help='path to directory or file containing source images')
    required.add_argument('--targets', required=True, nargs='+',
                          help='path to directory or file containing target images')
    required.add_argument('--model-dir', type=str, required=True,
                          help=('Directory where model definition files are saved during training. Models will be '
                                'saved as `model_dir`/`experiment_name`_{G_A,G_B,D_A,D_B}_epoch##.h5'))
    required.add_argument('--experiment-name', type=str, 
                          default='synthesis_'+datetime.datetime.now().strftime('%Y%m%d_%H%M%S'),
                          help=('Experiment name for use in naming model definition files. Models will be saved '
                                'as `model_dir`/`experiment_name`_{G_A,G_B,D_A,D_B}_epoch##.h5. Defaults to '
                                '"cyclegan_{CURRENT_DATE_TIME}".'))
    
    images = parser.add_argument_group('images')
    images.add_argument('--input-img-max', type=float, help='image max for input normalization')
    images.add_argument('--output-img-max', type=float, help='image max for output normalization')
    images.add_argument('--orientation', type=str, choices=['axial', 'coronal', 'sagittal'], default='axial',
                        help='For Nifti images, orient to this orientation before slicing')
    images.add_argument('--pad-size', type=int, nargs='+',
                        help='Pad images to this size, larger images will not be changed. The default behavior ')
    images.add_argument('--patch-size', type=int, nargs='+', default=(256, 256),
                        help='Crop out patches of this size, i.e. `128 128` for 128x128 2D patches or '
                             '`64 64 64` for 64x64x64 3D patches')
    images.add_argument('--z-res-change', type=str, choices=['all', 'half', 'none'], default='all',
                        help='for 3D patches, on how many levels will the z-direction be downsampled')
    # TODO: Implement shrink in training. Currently, we are just cropping after prediction.
    images.add_argument('--shrink', type=int, nargs='+', default=(0, 0, 0),
                        help='How much to shrink output image on each edge. For example, if a 128x128x3 patch is used, '
                             'a shrink of 0x0x1, would predict 128x128x1 patches. For 2D patches, the 3rd shrink value '
                             'is ignored, if given. This value should be odd. This will be implemented via a ')
    images.add_argument('--flip-images', action='store_true', default=False,
                        help='randomly flip images in the x direction')
    images.add_argument('--source-masks', type=str, default=None,
                        help='Mask of indices from which to sample for patch extraction for sources'
                             '(non-zero voxels are sampled if masks are not provided)')
    images.add_argument('--target-masks', type=str, default=None,
                        help='Mask of indices from which to sample for patch extraction for targets'
                             '(non-zero voxels are sampled if masks are not provided)')

    model = parser.add_argument_group('model')
    model.add_argument('--model-name', type=str, choices=['singlecovnet', 'pix2pix', 'cyclegan', 'cyclenogan'],
                       default='cyclegan', help='Type of model to fit. Choose one of: "singlecovnet", "pix2pix", '
                                                '"cyclegan", "cyclenogan".')
    model.add_argument('--channel-dropout', type=float, help='dropout input channels with this frequency')
    model.add_argument('--channel-protect', type=int, nargs='+', help='protect these channels from dropout',
                       default=(0,))
    
    generator = parser.add_argument_group('generator')
    generator.add_argument('--init-filters-gen', type=int, default=32, help='# of gen filters in first conv layer')
    generator.add_argument('--generator-name', type=str, default='unet', choices=['resnet', 'unet'],
                           help='selects model to use for generator network')
    generator.add_argument('--generator-levels', type=int, default=4,
                           help='number of downsample levels in the generator')
    generator.add_argument('--generator-blocks', type=int, default=2,
                           help='number of blocks in the bottleneck for resnet or per level in unet')
    generator.add_argument('--generator-loss', type=str, choices=['MAE', 'MSE'], default='MAE',
                           help='loss function for generator-based comparisons (cycle consistency for CycleGAN)')
    generator.add_argument('--norm-method-gen', type=str, choices=['instance', 'batch'], default='instance',
                           help='normalization method for generator (instance or batch normalization)')
    generator.add_argument('--padding-gen', type=str, choices=['zero', 'reflect'], default='zero',
                           help='padding method for generator (zero or reflection padding)')
    generator.add_argument('--upsample-method-gen', type=str, choices=['deconv', 'upsample'], default='upsample',
                           help='method to use for upsampling in the generator. "deconv" uses the Conv2DTranspose '
                                'layer, while "upsample" uses Upsampling2D followed by Conv2D')
    generator.add_argument('--downsample-method-gen', type=str, choices=['max_pool', 'avg_pool', 'strided'],
                           default='max_pool', help='method to use for downsampling in the generator. "max_pool" '
                                                    'uses the MaxPooling2D layer followed by a convolution, while '
                                                    '"strided" uses Conv2D with stride of (2, 2)')
    generator.add_argument('--conv-activation-gen', type=str, choices=['relu', 'leaky_relu'], default='relu',
                           help='activation after convolution for generator')
    generator.add_argument('--kernel-initializer-gen', type=str, choices=['he_normal', 'glorot_uniform',
                                                                          'random_normal'],
                           default='he_normal', help='kernel initializer for convolution in the generator')
    generator.add_argument('--skip-connections-gen', type=str, choices=['all', 'unet', 'none'], default='all',
                           help='Number of skip connections for UNet generators. "all" skip connects all levels '
                                'including input level. "unet" connects all but the input level. "none" does not use '
                                'skip connections (auto-encoder).')
    generator.add_argument('--resblocks', action='store_true', default=False,
                           help='In the U-Net architecture, create a residual block at each level from the (multiple) '
                                'level convolutions.')
    generator.add_argument('--gen-final-activation', type=str,
                           choices=['linear', 'relu', 'normalized-relu', 'sigmoid', 'tanh', 'leaky_relu'],
                           default='relu', help='final activation function for generator '
                                                '(determines image normalization)')
    generator.add_argument('--use-identity-loss', action='store_true', default=False,
                           help='In CycleGAN, use identity mapping to ensure that G_A(B) produces B.')
    generator.add_argument('--lambda-id', type=float, default=1.0,
                           help=('Identity mapping weight. Setting lambda_id other than 1 has an effect of scaling '
                                 'the weight of the identity mapping loss. This weight is combined with lambda_a and '
                                 'lambda_b to create the total identity weight for each direction '
                                 '(lambda_id_a = lambda_a * lambda_id). For example, if the weight of '
                                 'the identity loss should be 10 times smaller than the weight of the '
                                 'reconstruction loss, please set `lambda_id` = 0.1'))

    discriminator = parser.add_argument_group('discriminator')
    discriminator.add_argument('--init-filters-dis', type=int, default=64,
                               help='# of discriminator filters in first conv layer')
    discriminator.add_argument('--num-layers-dis', type=int, default=3,
                               help='number of layers in discriminator network (PatchGAN)')
    discriminator.add_argument('--dis-model-type', type=str, choices=['patchgan', 'imagegan'], default='patchgan',
                               help='Either PatchGAN or ImageGAN.')
    discriminator.add_argument('--norm-method-dis', type=str, choices=['instance', 'batch'], default='instance',
                               help='normalization method for discriminator (instance or batch normalization)')
    discriminator.add_argument('--conv-activation-dis', type=str, choices=['relu', 'leaky_relu'], default='relu',
                               help='activation after convolution for discriminator')
    discriminator.add_argument('--kernel-initializer-dis', type=str, choices=['he_normal', 'glorot_uniform',
                                                                              'random_normal'],
                               default='he_normal', help='kernel initializer for convolution in the discriminator')
    discriminator.add_argument('--gan-type', type=str, choices=['DCGAN', 'LSGAN', 'WGAN', 'WGAN-GP', 'WGAN-LP'],
                               default='DCGAN', help='The type of GAN training to use (choice of discriminator/critic)')
    discriminator.add_argument('--fuzzy-labels', action='store_true', default=False,
                               help='Instead of using constant truth labels ([0,1] for vanilla and ls-gan or [-1,1] '
                                    'for wgan), use random values close to truth values.')
    discriminator.add_argument('--lambda-a', type=float, default=10.0,
                               help='Weight of cycle loss for distribution A. For Pix2Pix, this weights the L1 loss.')
    discriminator.add_argument('--lambda-b', type=float,  help='Weight of the cycle loss for distribution B. This '
                                                               'defaults to the same value as lambda-a.')
    discriminator.add_argument('--lambda-gp', type=float, default=10.0,
                               help='For improved WGAN, this is the gradient penalty weight')
    discriminator.add_argument('--discrim-iter', type=int, default=1, help='number of batches to train the '
                                                                           'discriminator for each training batch of '
                                                                           'the generator')
    discriminator.add_argument('--pretrain-iter', type=int, default=0,
                               help='the number of pretraining batches to run on the discriminator '
                                    'model(s) at the beginning of each epoch')
    discriminator.add_argument('--skip-first-pretrain', action='store_true', default=False,
                               help='skip the first pretraining step before Epoch 1')
    
    training = parser.add_argument_group('training')
    training.add_argument('--n-epochs', type=int, default=100, help='# of epochs at starting learning rate')
    training.add_argument('--n-epochs-decay', type=int, default=0,
                          help='# of epochs to linearly decay learning rate to zero')
    training.add_argument('--steps-per-epoch', type=int, default=10000,
                          help='number of steps (batches) to run for each epoch')
    training.add_argument('--batch-size', type=int, default=1, help='number of images used in each step (batch)')
    training.add_argument('--starting-epoch', type=int, default=1,
                          help='(the starting epoch count, we save the model by <starting_epoch>, '
                               '<starting_epoch>+<save_latest_freq>, ...')
    training.add_argument('--pool-size', type=int, default=50,
                          help=('the size of image buffer that stores previously generated images '
                                'for discrimnator training'))
    training.add_argument('--image-access', type=str, choices=['random', 'structured'], default='random',
                          help='During unpaired training, determine how images are accessed')
    training.add_argument('--random-orientation', action='store_true', default=False,
                          help='randomize the orientation of training slices')
    training.add_argument('--structured-shift-range', type=int, nargs='+', default=(5,),
                          help='During structured training, pick slices within this range of the original slice')
    training.add_argument('--optimizer', type=str, choices=['Adam', 'RMSprop'], default='Adam',
                          help='Optimizer for gradient descent')
    training.add_argument('--learning-rate', type=float, default=0.001, help='initial learning rate for optimizer')
    training.add_argument('--beta1', type=float, default=0.9, help='momentum term of optimizer '
                                                                   '(called rho for RMSprop)')
    training.add_argument('--beta2', type=float, default=0.999, help='second momentum term of adam')
    training.add_argument('--continue-training', action='store_true', default=False,
                          help=('Continue training? Loads models from `model_dir` to continue training. '
                                'Loading an existing model overwrites any options in the `model` group (except '
                                '`model_name`).'))
    training.add_argument('--experiment-to-load', type=str,
                          help='Experiment name to load (if different from `experiment_name`)')
    training.add_argument('--which-epoch', type=str, default='latest',
                          help='Which epoch to load? Set to "latest" to use latest saved model')
    training.add_argument('--continue-gen-only', action='store_true', default=False, help='Only load generator models.')
    training.add_argument('--print-freq', type=int, default=100,
                          help='frequency of showing training results on console (in steps/batches)')
    training.add_argument('--save-epoch-freq', type=int, default=5,
                          help='frequency of saving models at the end of epochs')
    training.add_argument('--gpu-ids', type=int, nargs='+', help='IDs for the GPU devices to use. These devices must'
                                                                 ' be visable to this process.')
    training.add_argument('--num-gpus', type=int, default=1, help='Use this number of GPUs. This will choose the first '
                                                                  'N GPU IDs from CUDA_VISABLE_DEVICES.')
    training.add_argument('--cpu-merge', action='store_true', default=False, help='merge multi-gpu weights on cpu')
    
    args = parser.parse_args()
    
    args.command = 'train.py ' + ' '.join(sys.argv[1:])

    args.input_nc = len(args.sources)
    args.output_nc = len(args.targets)

    if args.experiment_to_load is None:
        args.experiment_to_load = args.experiment_name
        
    orientations = {'axial': 'RAI', 'coronal': 'RSA', 'sagittal': 'ASR'}
    args.orientation = orientations[args.orientation]
    if args.random_orientation:
        args.orientation = 'RAI'
        
    if args.flip_images and args.orientation == 'ASR':
        warnings.warn('Flipping images is done in the x-direction, which in sagittal patches changes the orientation '
                      'of the brain. This may not be the desired effect.')
    
    available_gpus = [int(gpu_id) for gpu_id in os.environ['CUDA_VISIBLE_DEVICES'].split(',')]
    if args.gpu_ids is not None:
        if any([gpu_id not in available_gpus for gpu_id in args.gpu_ids]):
            raise RuntimeError('One or more GPU ID(s) specified is not available in CUDA_VISIBLE_DEVICES. Exiting.')
        args.num_gpus = len(args.gpu_ids)
    elif args.num_gpus > 1:
        if args.num_gpus > len(available_gpus):
            raise RuntimeError('Only %d GPUs are visible, you selected to use %d GPUs. Exiting' %
                               (len(available_gpus), args.num_gpus))
        args.gpu_ids = available_gpus[:args.num_gpus]
    
    models = {'cyclegan': CycleGAN, 'cyclenogan': CycleNoGAN, 'pix2pix': Pix2Pix, 'singlecovnet': SingleCovNet}
    model = models[args.model_name](**vars(args))
    
    if args.model_name in ['cyclegan', 'pix2pix']:
        if args.norm_method_dis == 'batch':
            args.norm_method_dis = 'batch-gan'
        if args.norm_method_gen == 'batch':
            args.norm_method_gen = 'batch-gan'
    
    access = 'paired' if args.model_name not in ['cyclegan'] else args.image_access
    if len(args.structured_shift_range) == 1:
        args.structured_shift_range = (args.structured_shift_range[0], args.structured_shift_range[0])
        
    if args.lambda_b is None:
        args.lambda_b = args.lambda_a

    ext, is_dir = get_files_extension(args.sources[0])
    if ext.lower() in NiftiGenerator.EXTENSIONS:
        if not is_dir:
            raise ValueError('"sources" and "targets" arguments must be directories of nifti files.')
        img_generator_a = NiftiGenerator(root_dirs=args.sources, training_orient=args.orientation,
                                         pad_size=args.pad_size, patch_size=args.patch_size, flip=args.flip_images,
                                         access=access, random_orient=args.random_orientation,
                                         gen_final_activation=args.gen_final_activation,
                                         img_max=args.input_img_max, masks=args.source_masks,
                                         shift_range=args.structured_shift_range,
                                         channel_dropout=args.channel_dropout,
                                         channel_protect=args.channel_protect)
    
        img_generator_b = NiftiGenerator(root_dirs=args.targets, training_orient=args.orientation,
                                         pad_size=args.pad_size, patch_size=args.patch_size, flip=args.flip_images,
                                         access=access, random_orient=args.random_orientation,
                                         gen_final_activation=args.gen_final_activation,
                                         img_max=args.output_img_max, masks=args.target_masks, master=img_generator_a,
                                         shift_range=args.structured_shift_range,
                                         channel_dropout=args.channel_dropout,
                                         channel_protect=args.channel_protect)
    else:
        raise ValueError('"sources" and "targets" arguments not valid filetypes (%s).' % ext)
        
    model.connect_inputs(input_a=img_generator_a, input_b=img_generator_b)
    
    model.fit(args.batch_size, args.n_epochs, args.n_epochs_decay, args.steps_per_epoch, args.save_epoch_freq,
              args.print_freq, args.starting_epoch)
