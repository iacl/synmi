# SynMI
A [Keras](keras.io) implementation of a number of Deep Learning tools for the synthesis of Medical Images (mainly MR or CT). Currently, Tensorflow is the required backend, but only one layer (ReflectionPadding) requires it.

## Models
There are 4 different models that can be used for training: **SingleCovNet**, **CycleNoGAN**, **Pix2Pix**, and **CycleGAN**. Each model is network agnostic and can be used with any of the available features (2D/3D, etc.).

#### SingleCovNet
A simple deep learning model for the comparison of paired data. This model trains by directly comparing input and output patches. Loss function can be controlled using the `--generator-loss` option.

#### CycleNoGAN
This model extends the **SingleCovNet** model to provide bi-directional synthesis with cyclic consistency, i.e. $`G_A(G_B(A))\approx{A}`$.

#### Pix2Pix
```
@article{pix2pix2017,
  title={Image-to-Image Translation with Conditional Adversarial Networks},
  author={Isola, Phillip and Zhu, Jun-Yan and Zhou, Tinghui and Efros, Alexei A},
  journal={CVPR},
  year={2017}
}
```

#### CycleGAN
```
@article{CycleGAN2017,
  title={Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks},
  author={Zhu, Jun-Yan and Park, Taesung and Isola, Phillip and Efros, Alexei A},
  journal={arXiv preprint arXiv:1703.10593},
  year={2017}
}

@article{pix2pix2016,
  title={Image-to-Image Translation with Conditional Adversarial Networks},
  author={Isola, Phillip and Zhu, Jun-Yan and Zhou, Tinghui and Efros, Alexei A},
  journal={arxiv},
  year={2016}
}
```


## Networks
There are 2 base networks that can be used for image generation: **U-Net** and **ResNet**. Each of these can be customized using the command line options. For example, the depth (number of downsamples) of the network can be set with the `n-levels` parameter. For the discriminator of GAN networks, a multi-level convolutional network (**PatchGAN**) is available.

#### U-Net

#### ResNet

#### PatchGAN

## GANs
Generative Adversarial Networks are available via Pix2Pix or CycleGAN and can be used with 4 different critics/discriminators: **DCGAN**, **LSGAN**, **WGAN-GP**, and **WGAN-LP**.

#### DCGAN

#### LSGAN

#### WGAN-GP

#### WGAN-LP

## Training Options
SynMI has a number of options to alter the training of a deep network, including: **Patch Size**, **Random Orientation**, **Discriminator Pre-Training**, **Training Continuation**, **Image Access**.

#### Patch Size
This sets the patch size for training. Patches will be randomly selected by selecting a non-zero voxel and creating a patch with that voxel at the center (padding if necessary). **Patch Size** also controls 2D/3D training. For example, `64 64` will create a 2D network with 64x64 patches, while `64 64 64` will create a 3D network with 64x64x64 patches.

#### Random Orientation

#### Discriminator Pre-Training

#### Training Continuation

#### Image Access

