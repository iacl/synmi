import os
import math

import numpy as np
from PIL import Image

from .utils import mkdir_p


class HTML:
    def __init__(self, web_dir, title):
        self.title = title
        self.web_dir = web_dir
        self.img_dir = os.path.join(self.web_dir, 'images')
        mkdir_p(self.img_dir)
        self.doc = '<!DOCTYPE html>\n<html>\n<head>\n<title>%s</title><body>\n' % self.title

    def add_header(self, string):
        self.doc += '<h3>' + string + '</h3>\n'

    def begin_table(self, border=1):
        self.doc += '<table border="%s" style="table-layout: fixed;">\n' % str(border)
    
    def end_table(self):
        self.doc += '</table>\n'
    
    def begin_row(self):
        self.doc += '<tr>\n'
    
    def end_row(self):
        self.doc += '</tr>\n'

    def begin_cell(self):
        self.doc += '<td style="word-wrap: break-word;" halign=center valign=top>\n'

    def end_cell(self):
        self.doc += '</td>\n'
        
    def br(self):
        self.doc += '<br />\n'

    def add_images(self, ims, txts, links):
        self.begin_table()
        self.begin_row()
        for im, txt, link in zip(ims, txts, links):
            self.begin_cell()
            self.doc += '<p><a href="%s"><img src="%s" /></a></p>\n' % (os.path.join('images', im),
                                                                        os.path.join('images', im))
            self.br()
            self.doc += '<p>%s</p>\n' % txt
            self.end_cell()
        self.end_row()
        self.end_table()

    def save(self):
        doc = self.doc + '</body>\n</html>'
        html_file = '%s/index.html' % self.web_dir
        f = open(html_file, 'wt')
        f.write(doc)
        f.close()


def save_image(image_numpy, image_path):
    if len(image_numpy.shape) == 3:
        image_numpy = image_numpy.reshape(image_numpy.shape[:2] + (1,) + (image_numpy.shape[2],))
    
    slices = int(math.ceil(math.sqrt(image_numpy.shape[2])))
    contrasts = int(math.ceil(math.sqrt(image_numpy.shape[3])))
    size = slices * contrasts
    if size > 1:
        montage_image = np.zeros((image_numpy.shape[0]*size, image_numpy.shape[1]*size))
        for i in range(contrasts):
            for j in range(contrasts):
                contrast_idx = i * contrasts + j
                contrast_arr = np.zeros((image_numpy.shape[0] * slices, image_numpy.shape[1] * slices))
                if contrast_idx < image_numpy.shape[3]:
                    for n in range(slices):
                        for m in range(slices):
                            slice_idx = n * slices + m
                            slice_arr = np.zeros(image_numpy.shape[:2])
                            if slice_idx < image_numpy.shape[2]:
                                slice_arr = image_numpy[:, :, slice_idx, contrast_idx]
                            contrast_arr[n * image_numpy.shape[0]:(n + 1) * image_numpy.shape[0],
                                         m * image_numpy.shape[1]:(m + 1) * image_numpy.shape[1]] = slice_arr
                    contrast_arr = contrast_arr - np.min(contrast_arr)
                    contrast_arr = contrast_arr / np.max(contrast_arr) * 255.0
                montage_image[i * image_numpy.shape[0] * slices:(i+1) * image_numpy.shape[0] * slices,
                              j * image_numpy.shape[1] * slices:(j+1) * image_numpy.shape[1] * slices] = contrast_arr
        image_numpy = montage_image
    else:
        image_numpy = np.squeeze(image_numpy)
        image_numpy = image_numpy - np.min(image_numpy)
        image_numpy = image_numpy / np.max(image_numpy) * 255.0
    image_pil = Image.fromarray(np.transpose(image_numpy.astype(np.uint8), [1, 0]))
    image_pil.save(image_path)


def save_training_page(model_dir, experiment_name, visuals, epoch):
    web_dir = os.path.join(model_dir, experiment_name + '_web')
    webpage = HTML(web_dir, 'SynMI Experiment = %s' % experiment_name)
    for label, image_numpy in visuals.items():
        img_path = os.path.join(web_dir, 'images', 'epoch%.3d_%s.png' % (epoch, label))
        save_image(image_numpy, img_path)
    for n in range(epoch, 0, -1):
        webpage.add_header('epoch [%d]' % n)
        ims = []
        txts = []
        links = []
        
        for label, image_numpy in visuals.items():
            img_path = 'epoch%.3d_%s.png' % (n, label)
            ims.append(img_path)
            txts.append(label)
            links.append(img_path)
        webpage.add_images(ims, txts, links)
    webpage.save()
