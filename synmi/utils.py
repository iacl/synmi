import os

from keras import backend


def get_input_shape(patch2d, num_channels):
    image_size = (None, None) if patch2d else (None, None, None)
    return ((num_channels,) + image_size) if backend.image_data_format() == 'channels_first' \
        else (image_size + (num_channels,))


def get_channel_axis():
    return 1 if backend.image_data_format() == 'channels_first' else -1


def get_image_data_format():
    return backend.image_data_format()


def split_filename(input_path):
    dirname = os.path.dirname(input_path)
    basename = os.path.basename(input_path)
    
    base_arr = basename.split('.')
    ext = ''
    if len(base_arr) > 1:
        ext = base_arr[-1]
        if ext == 'gz':
            ext = '.'.join(base_arr[-2:])
        ext = '.' + ext
        basename = basename[:-len(ext)]
    return dirname, basename, ext


def get_files_extension(path):
    if os.path.isdir(path):
        return split_filename(os.listdir(path)[0])[2], True
    else:
        return split_filename(path)[2], False


# http://stackoverflow.com/a/22718321
def mkdir_p(path):
    import os
    import errno
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def reorient_3d_image(image_object, target_orient):
    import nibabel as nib
    import numpy as np

    orient_dict = {'R': 'L', 'A': 'P', 'I': 'S', 'L': 'R', 'P': 'A', 'S': 'I'}

    target_orient = [orient_dict[char] for char in target_orient]
    if nib.aff2axcodes(image_object.affine) != tuple(target_orient):
        orig_ornt = nib.orientations.io_orientation(image_object.affine)
        targ_ornt = nib.orientations.axcodes2ornt(target_orient)
        ornt_xfm = nib.orientations.ornt_transform(orig_ornt, targ_ornt)
        return nib.orientations.apply_orientation(image_object.dataobj, ornt_xfm).astype(np.float32)
    else:
        return image_object.get_data().astype(np.float32)


def writeline(file_obj, line):
    file_obj.write(line + '\n')